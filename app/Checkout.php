<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Checkout extends Model
{
    protected $fillable = ['name','order_id','address','email','phone','paymentMethod','transactionnumber','status'];

}
