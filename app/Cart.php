<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cart extends Model
{
    protected $fillable = ['sesstion_id', 'product_id','product_quantity','product_price'];
}
