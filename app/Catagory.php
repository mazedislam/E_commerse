<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Catagory extends Model
{
    protected $fillable = ['title', 'picture'];


    public function product()
    {
       return $this->hasMany(Product::class);
    }
}
