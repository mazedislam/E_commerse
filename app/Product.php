<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $fillable = ['catagory_id', 'title','picture','short_description','description','additional_information','price','special_price','offer','start_date','end_date','sku','meta_keyword','meta_description','product_url'];

    public function category()
    {
        return $this->belongsTo(Catagory::class);
    }
}
