<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderItem extends Model
{
    protected $table='order_items';
    protected $fillable = ['order_id','product_id','product_quantity','product_price'];
}
