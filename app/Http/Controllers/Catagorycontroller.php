<?php

namespace App\Http\Controllers;

use App\catagory;
use Illuminate\Http\Request;

class Catagorycontroller extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public $path='admin/catagory/';
    public function index()
    {
        $catagorys =Catagory::all();
        return view($this->path.'index', compact('catagorys'));
    }
    public function catagoryList()
    {
        $catagorys =Catagory::all();
        return view('user/catagory/catagorylist', compact('catagorys'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        return view($this->path.'create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $picture_name = time()."-".$request->picture->getClientOriginalName();
        $request->picture->move(public_path('picture/'),$picture_name);

        $allData = $request->all();
        $allData['picture']=$picture_name;
        Catagory::create($allData);

        return redirect('/catagory');
    }
    public function catagoryProduct($id)
    {
        $catagoryproducts=Catagory::find($id)->product;

        return view('user/catagory/catagoryproduct',compact('catagoryproducts'));

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($title)
    {
        $catagory=Catagory::find($title);
        return view($this->path.'show',compact('catagory'));

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $edit = Catagory::find($id);

        return view($this->path.'edit',compact('edit'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $existing = Catagory::find($id);

        $updatedData = $request->all();

        if (isset($updatedData['picture'])){
            $picture_name = time()."-".$request->picture->getClientOriginalName();

            $request->picture->move(public_path('picture'),$picture_name);
            $updatedData['picture'] = $picture_name;
        }
        else{
            $updatedData['picture']=$existing->picture;
        }

        $existing->update($updatedData);

       return redirect('/catagory')->withMessage('Catagory Updated !');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Catagory::find($id)->delete();

        return  redirect('/catagory')->withMessage('Catagory  Delete!');
    }
}
