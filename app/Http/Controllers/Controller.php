<?php

namespace App\Http\Controllers;

use App\Cart;
use App\navigation;
use App\page;
use App\Product;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\View\View;
use Illuminate\Support\Facades\Session;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

   public $guestid=null;
     public function __construct()
    {
        $pages=page::all();
        \View::share('pages',$pages);

        $navigations=navigation::all();
        \View::share('navigations',$navigations);

        $carts=Cart::all();
        \View::share('carts',$carts);





        session_start();


        if(array_key_exists('session_id',$_SESSION))
        {
            $this->guestid=$_SESSION['session_id'];
        }


        if (is_null($this->guestid)) {
            $_SESSION['session_id']=time().'-'.rand();
       }



       $id=$this->guestid;


        $productids=Cart::select()->where('sesstion_id','=',$id)->pluck('product_id');

        $products=Product::find($productids);

        \View::share('products',$products);


        \View::share('id',$id);




    }

}
