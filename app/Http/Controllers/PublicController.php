<?php

namespace App\Http\Controllers;

use App\Catagory;
use App\Slider;
use Illuminate\Http\Request;

class PublicController extends Controller
{
    public function index()
    {
        $sliders = Slider::all();
        $catagorys = Catagory::all();

        return view('user/home', compact('sliders','catagorys'));

    }
}
