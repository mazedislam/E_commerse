<?php

namespace App\Http\Controllers;

use App\slider;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Intervention\Image\ImageManagerStatic as Image;

class Slidercontroller extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     *
     *TODO
     * / DIRECTORY_SEPARATOR
     */
    const UPLOAD_DIR = '/uploads/slider-image/';
    public $path='admin/slider_index/';
    public function index()
    {
        $sliders = Slider::latest()->get();
        return view($this->path.'index', compact('sliders'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view($this->path.'create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $data = $request->all();
        if ($request->hasFile('picture')){
            $file = $request->file('picture');
            $data['picture'] = $this->uploadImage($file);
        }else{
            $data['picture'] = null;
        }
        Slider::create($data);
        //checking if success or failure
        return redirect('sliders')->withMessage('Slider Added !');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $slider = Slider::findOrFail($id);
        return view($this->path.'show', compact('slider'));
    }


    public function edit($id)
    {
        $edit = Slider::findOrFail($id);
        return view($this->path.'edit', compact('edit'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $existing = Slider::findorfail($id);

        $updatedData = $request->all();

        if (isset($updatedData['picture'])){
            $picture_name = time()."-".$request->picture->getClientOriginalName();

            $request->picture->move(public_path('uploads/slider-image'),$picture_name);
            $updatedData['picture'] = $picture_name;
        }
        else{
            $updatedData['picture']=$existing->picture;
        }

        $existing->update($updatedData);


        return redirect('sliders')->withMessage('Slider Updated !');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Slider::destroy($id);
        return redirect('sliders')->withMessage('Slider Deleted !');
    }
    private function uploadImage($file)
    {
        $timestamp = str_replace([' ', ':'], '-', Carbon::now()->toDateTimeString());
        $image_file_name = $timestamp . '.' . $file->getClientOriginalExtension();
        Image::make($file)->resize(900,400)->save(public_path() . self::UPLOAD_DIR . $image_file_name);
        return $image_file_name;
    }

    public function slider_list()
    {
        $alldata = Slider::latest()->get();
        return view($this->path.'index', compact('alldata'));
    }

}
