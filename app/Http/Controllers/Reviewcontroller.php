<?php

namespace App\Http\Controllers;

use App\review;
use Illuminate\Http\Request;

class Reviewcontroller extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     *
     *
     */
    public $path='admin/reviews/';
    public function index()
    {
        $reviews=Review::all();
        return view($this->path.'index',compact('reviews'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view($this->path.'create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $review=new review();
        $review->product_id=$request->product_id;
        $review->name=$request->name;
        $review->Email=$request->Email;
        $review->rating=$request->rating;
        $review->review_title=$request->review_title;
        $review->body_review=$request->body_review;



        $review->save();
        return redirect($_SERVER['HTTP_REFERER']);
      /*  return redirect('/reviews')->withMessage('Reviews created!!!!');*/
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      $edit=Review::find($id);

      return view($this->path.'edit',compact('edit'));
      return redirect('reviews');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $update=Review::find($id);
        $update->product_id=$request->product_id;
        $update->name=$request->name;
        $update->Email=$request->Email;
        $update->start_date=$request->start_date;
        $update->review_title=$request->review_title;
        $update->body_review=$request->body_review;

        $update->update();

        return redirect('/reviews');



    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $delete=Review::find($id);
        $delete->delete();
        return redirect('/reviews');
    }
}
