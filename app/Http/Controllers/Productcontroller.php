<?php

namespace App\Http\Controllers;

use App\Catagory;
use App\product;
use App\Review;
use Illuminate\Http\Request;

class Productcontroller extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public $path='admin/products/';

    public function index()
    {

        $products=Product::all();
        return view($this->path.'index',compact('products'));
    }

    public function productList()
    {
        $allproducts=Product::latest()->paginate(6);


        return view('user/products/product_listing',compact('allproducts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories=Catagory::orderBy('title','asc')->pluck('title','id');
        return view('admin/products/create',compact('categories'));
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $alldata=$request->all();

        $picture_name = time()."-".$request->picture->getClientOriginalName();
        $request->picture->move(public_path('product_image/'),$picture_name);
        $alldata['picture']=$picture_name;

        Product::create($alldata);
        return redirect('/products')->withMessage('Slider Added !');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $product=Product::find($id);
        $reviewProducts=Review::select()->where('product_id',$id)->get();
        return view('user/products/view',compact('product','reviewProducts'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $edit=Product::find($id);
        $categories=Catagory::orderBy('title','asc')->pluck('title','id');
        return view($this->path.'edit',compact('edit','categories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $exit=Product::find($id);
        $data=$request->all();
        if(isset($data['picture']))
        {
            $picturename=time()."-".$request->picture->getClientOriginalName();
            $request->picture->move(public_path('product_image/'),$picturename);
            $data['picture']=$picturename;
        }
        else {
            $data['picture'] = $exit->picture;
        }
        $exit->update($data);
        return redirect('products')->withMessage('Product Updated !');


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $delete=Product::find($id);
        $delete->delete();
        return redirect('products')->withMessage('Product  Delete!');
    }

    public function offerZone()
    {
        $offers=Product::select()->where('offer','offer')->get();
        return view('user/products/offerzone',compact('offers'));
    }

}
