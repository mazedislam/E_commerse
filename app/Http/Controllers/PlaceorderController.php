<?php

namespace App\Http\Controllers;

use App\Cart;
use App\Mail\OrderConfirmed;
use App\OrderItem;
use App\PlaceOrder;
use App\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class PlaceorderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public $path='admin/placeorder/';
    public function index()
    {
        $placeorders=PlaceOrder::all();
        return view($this->path.'checkout_index',compact('placeorders'));
    }



    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view("admin/placeorder/create");
    }

    public function message()
    {
        return view("admin/placeorder/message");
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       PlaceOrder::create($request->all());


        $cartItems=Cart::select()->where('sesstion_id',$_SESSION['session_id'])->get();
        $productId=Cart::select()->where('sesstion_id',$_SESSION['session_id'])->pluck('product_id');
        $prodductInfo=Product::find($productId);


        foreach ($cartItems as $cart)
        {
            $orderItem['order_id']=$request->order_id;
            $orderItem['product_id']=$cart->product_id;
            $orderItem['product_quantity']=$cart->product_quantity;
            $orderItem['product_price']=$cart->product_price;

          /*  DB::table('order_items')->insert($orderItem);*/
            OrderItem::create($orderItem);
            $cart->delete();

        }
      $this->mobileSms($request->phone,' Mr '.$request->name. ' Your Order Confirm');

      \Mail::to($request->email)->send(new OrderConfirmed($cartItems,$prodductInfo));
       session(['order_id'=>$request->order_id]);

        return redirect("/message");

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

        $orderitems=OrderItem::select()->where('order_id',$id)->get();
        $productId=OrderItem::select()->where('order_id',$id)->pluck('product_id');
        $productInfo=Product::find($productId);

        $singleOrder=PlaceOrder::select()->where('order_id',$id)->first();


        return view('admin/placeorder/singleorder',compact('singleOrder','orderitems','productInfo'));
    }

    public function mobileSms($phoneNumber,$message)
    {

        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => "http://api.infobip.com/sms/1/text/single",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => "{ \"from\":\"InfoSMS\", \"to\":\"$phoneNumber\", \"text\":\"$message\" }",
            CURLOPT_HTTPHEADER => array(
                "accept: application/json",
                "authorization: Basic YmFyY29kZXNvbHV0aW9uOkxORUsxYXg0eDEy",
                "content-type: application/json"
            ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);
 /*  if ($err) {
       echo "cURL Error #:" . $err;
   } else {
       echo $response;
   }
    die();*/
    }

    public function myOrder($order_id)
    {
        $order=PlaceOrder::select()->where('order_id',$order_id)->first();

        return view('admin/placeorder/vieworder',compact('order'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function update(Request $request, $id)
    {

        $iddata=PlaceOrder::find($id);
        {
            $status=$request->all();
            $iddata->update($status);
            return redirect('/placeorder');

        }


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

}
