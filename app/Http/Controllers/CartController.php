<?php

namespace App\Http\Controllers;

use App\Cart;
use App\Product;
use App\Review;
use Illuminate\Http\Request;

class CartController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public $path='user/Cart/';
    public function index()
    {

        $carts=Cart::all();
        $productids=Cart::select()->where('sesstion_id','=',$this->guestid)->pluck('product_id');
        $products=Product::find($productids);


        if(count($carts)>0) {
            return view($this->path . 'index', compact('carts', 'products'));
        }
        else if(count($carts)==0)
        {
            return view($this->path.'empty');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view($this->path.'create');

    }
    public function createCart($id)
    {
        $product=Product::find($id);
        $reviewProducts=Review::select()->where('product_id',$id)->get();

        return view('user/products/view',compact('product','reviewProducts'));

    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function store(Request $request)
    {
       $cartCollections = Cart::where([['sesstion_id','=',$request->sesstion_id],['product_id','=',$request->product_id]])->get();
        if($cartCollections->count()>0){
            $cartCollection=Cart::where([['sesstion_id','=',$request->sesstion_id],['product_id','=',$request->product_id]])->first();
            $id=$cartCollection->id;
            $totalquantiy= $cartCollection->product_quantity+$request->product_quantity;
            $totalamount=$totalquantiy*$request->product_price;
            $cart = $request->all();
            $cart['product_price']=$totalamount;
            $cart['product_quantity']=$totalquantiy;
            $exit=Cart::find($id);
            $exit->update($cart);
            return redirect('carts');
        }
        else {
            $carts = $request->all();
            Cart::create($carts);
            return redirect('/carts');
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $cartview=Cart::find($id);
        return view($this->path.'show',compact('cartview'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $cartedit=Cart::find($id);

        $product_id=Product::select()->where('id',$cartedit->product_id)->first();
        return view($this->path.'edit',compact('cartedit','product_id'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $cartid=Cart::find($id);

        if($request->product_quantity<1)
        {
            session()->flash('message','Quantity selecet 1 or 1 more than!!!');
            return redirect('carts');
        }

        else{
            $data=$request->all();
            $cartid->update($data);
            return redirect('/carts');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
       $cartid=Cart::find($id);
       $cartid->delete();
       return redirect('/carts');
    }
}
