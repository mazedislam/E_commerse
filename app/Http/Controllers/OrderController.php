<?php

namespace App\Http\Controllers;

use App\Cart;
use App\OrderItem;
use App\PlaceOrder;
use App\Product;
use PDF;
use Illuminate\Http\Request;

class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {


    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

        $productids=OrderItem::select()->where('order_id','=',$id)->pluck('product_id');
        $Products=Product::find($productids);

        $orderItems=OrderItem::select()->where('order_id','=',$id)->get();

        return view('admin/orderitem/orderitem',compact('orderItems','Products'));

    }
    public function  pdfInvoice($orderId)
    {


        $productids=OrderItem::select()->where('order_id','=',$orderId)->pluck('product_id');

        $orderProducts=Product::find($productids);

        $userInfo=PlaceOrder::select()->where('order_id','=',$orderId)->first();

        $orderItems=OrderItem::select()->where('order_id','=',$orderId)->get();


        $pdf = PDF::loadView('admin/orderitem/pdfinvoice',compact('orderItems','orderProducts','userInfo'));
    /*    $pdf = PDF::loadView('admin/orderitem/pdfinvoice');*/

        return $pdf->stream('invoice_'.$orderId.'.pdf');

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
