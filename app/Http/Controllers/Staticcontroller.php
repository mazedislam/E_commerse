<?php

namespace App\Http\Controllers;

use App\staticblock;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class Staticcontroller extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public $path='admin/staticblock/';

    public function index()
    {

        $staticblocks=Staticblock::all();
       return view($this->path.'index',compact('staticblocks'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
       return view($this->path.'create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $static=new staticblock;
        $static->title=$request->title;
        $static->body=$request->body;

        $static->save();
        return redirect('/staticblock');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $show=Staticblock::find($id);

        return view($this->path.'show',compact('show'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $edit=Staticblock::find($id);

        return view($this->path.'edit',compact('edit'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $static=Staticblock::find($id);
        $static->title=$request->title;
        $static->body=$request->body;

        $static->save();
        return redirect('/staticblock');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
       $static=Staticblock::find($id);
        $static->delete();
       return redirect('/staticblock');
    }
}
