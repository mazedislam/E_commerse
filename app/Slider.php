<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Slider extends Model
{
    protected $fillable = ['name','transition', 'picture', 'price','link','htmlblock','is_active'];
}
