<!DOCTYPE html>
<html lang="en-US">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, minimum-scale=1, maximum-scale=1">
    <meta name="keywords" content="HTML5 Template">
    <meta name="description" content="ShopHub">
    <meta name="author" content="etheme.com">
    {{--<link rel="shortcut icon" href="{{asset('favicon.png')}}">--}}
    <title>@yield('title')</title>

    <!-- STYLESHEET -->
    <!-- FONTS -->
    <!-- Muli -->
    <!-- icon -->
    <!-- Font Awesome -->
    <link href="{{asset('css/app.css')}}" rel="stylesheet">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Muli:300,400,600,700,800%7CMontserrat:300,400,500,600,700%7COpen+Sans">

    <link rel="stylesheet" href="{{ asset('fonts/icons/fontawesome/font-awesome.css') }}">
    <link rel="stylesheet" href="{{ asset('fonts/icons/myfont/css/myfont-embedded.css') }}">
    <link rel="stylesheet" href="{{ asset('fonts/icons/myfont/css/animation.css') }}">
    <link rel="stylesheet" href="{{ asset('fonts/icons/myfont/css/myfont.css') }}">
    <!--[if lt IE 9]>
    <link rel="stylesheet" href="{{ asset('fonts/icons/myfont/css/myfont-ie7.css') }}">

     <![endif]-->

    <!-- Vendor -->
    <link rel="stylesheet" href="{{asset('vendor/bootstrap/v3/css/bootstrap.css')}}">
    <link rel="stylesheet" href="{{asset('vendor/bootstrap/v4/css/bootstrap-grid.css')}}">
    <link rel="stylesheet" href="{{asset('vendor/perfectScrollbar/css/perfect-scrollbar.min.css')}}">
    <link rel="stylesheet" href="{{asset('vendor/tonymenu/css/tonymenu.css')}}">
    <link rel="stylesheet" href="{{asset('vendor/revolution/css/settings.css')}}">
    <link rel="stylesheet" href="{{asset('vendor/revolution/css/layers.css')}}">
    <link rel="stylesheet" href="{{asset('vendor/revolution/css/navigation.css')}}">
    <link rel="stylesheet" href="{{asset('vendor/slick/slick.min.css')}}">
    <link rel="stylesheet" href="{{asset('vendor/magnificPopup/dist/magnific-popup.css')}}">
    <link rel="stylesheet" href="{{asset('vendor/rangeSlider/css/ion.rangeSlider.css')}}">
    <link rel="stylesheet" href="{{asset('vendor/rangeSlider/css/ion.rangeSlider.skinFlat.css')}}">
    <link rel="stylesheet" href="{{asset('vendor/swiper/css/swiper.min.css')}}">
    <link rel="stylesheet" href="{{asset('vendor/fotorama/fotorama.css')}}">


    <!-- Custom -->
    @stack('pdfcss')

{{--    @yield('link')--}}

    <link rel="stylesheet" href="{{asset('css/style.css')}}">


    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>

    <![endif]-->
</head>
<body id="theme">
<!-- HEADER -->
<header>
    <div class="tt-preloader"></div>

    <div class="tt-header tt-header--build-01 tt-header--style-01 tt-header--sticky">
        <div class="tt-header__content">
            <div class="tt-header__logo" style="background:white">
                <div class="h1 tt-logo tt-logo__curtain">
                    <a href="{{url('/')}}">
                        <img src="{{asset('shopno.jpg')}}" style="width:248px;height: 86px;margin-top: -17px" alt="mogo">
                    </a>
                </div>
            </div>
            <div class="tt-header__nav">
                <div class="tt-header__menu">
                    <nav class="TonyM TonyM--header"
                         data-tm-dir="row"
                         data-tm-mob="true"
                         data-tm-anm="emersion">
                        <ul class="TonyM__panel">
                            @foreach($navigations as $navigation)
                                <li>
                                    <a href="{{url($navigation->title)}}">
                                        {{$navigation->title}}
                                   {{--     <i class="TonyM__arw"></i>--}}
                                    </a>
                                   {{-- <div class="TonyM__mm TonyM__mm--simple"
                                         data-tm-w="280"
                                         data-tm-a-h="item-left">
                                    </div>--}}
                                </li>
                            @endforeach
                        </ul>
                    </nav>
                </div>
                <div class="tt-header__sidebar">
                    <div class="tt-header__options">
                        <a href="#" class="tt-header__btn tt-header__btn-menu"><i class="icon-menu"></i></a>
                        <div role="search" class="tt-header__search">
                            <form action="#" class="tt-header__search-form">
                                <input type="search" name="q" class="form-control" placeholder="Search...">
                            </form>
                            <div class="tt-header__search-dropdown"></div>
                            <a href="#" class="tt-header__btn tt-header__btn-open-search"><i class="icon-search"></i></a>
                            <a href="#" class="tt-header__btn tt-header__btn-close-search"><i class="icon-cancel-1"></i></a>
                        </div>
                        <div>
                            <a href="#" class="tt-header__btn tt-header__btn-user"><i class="icon-user-outline"></i></a>
                            <div class="tt-header__user">
                                <ul class="tt-list-toggle">
                                    <li><a href="my-account">My account</a></li>
                                    <li><a href="checkout">Checkout</a></li>
                                    <li><a href="login">Login</a></li>
                                    <li><a href="register">Register</a></li>

                                    <div class="tt-header__login">
                                        <h6>Login</h6>
                                        <form action="#">
                                            <input type="email" class="form-control" placeholder="Email" required="required">
                                            <input type="password" class="form-control" placeholder="Password" required="required">
                                            <button type="submit" class="btn">Login</button>
                                            <div>
                                                <a href="account">Forget your password?</a>
                                            </div>
                                        </form>
                                    </div>
                                </ul>
                            </div>
                        </div>
                        <div>
                            <a href="" class="tt-header__btn tt-header__btn-cart">
                                <i class="icon-shop24"></i>
                                <span>{{count($products)}}</span>
                            </a>
                            <div class="tt-header__cart">
                                <div class="tt-header__cart-content">
                                    <h6 class="text-center">There is {{count($products)}} item in your bag</h6>
                                    <ul class="colorize-bd">
                                        <?php $sum=0; ?>
                                        @foreach($carts as $cart)
                                            @foreach($products as $product)
                                                @if($cart->product_id == $product->id)

                                                    <li>
                                                        <?php
                                                        $total=$product->price*$cart->product_quantity;
                                                        $sum=$sum+$total;
                                                        ?>
                                                        <div>
                                                            <a href="{{url('carts/'.$product->id.'/create')}}">
                                                                <img src="{{asset('product_image/'.$product->picture)}}" alt="image" class="colorize-theme2-bd colorize-theme-bd-h">
                                                            </a>
                                                        </div>
                                                        <div>
                                                            <p>
                                                                <a href="product">Elegant and fresh. A most attractive mobile power supply.</a>
                                                            </p>
                                                            <span class="tt-header__cart-price">
                                                    <span class="tt-header__cart-price-count">{{$cart->product_quantity}}</span>
                                                    <span>x</span>
                                                    <span class="tt-header__cart-price-val tt-price">{{$cart->product_price}}</span>
                                                </span>
                                                            <div class="tt-counter" data-min="1" data-max="99">
                                                                <form action="#">
                                                                    <input type="text" value="{{$cart->product_quantity}}" class="form-control">
                                                                </form>
                                                                <div class="tt-counter__control">
                                                                    <span class="icon-up-circle" data-direction="next"></span>
                                                                    <span class="icon-down-circle" data-direction="prev"></span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div>
                                                            <a href="{{url('carts/'.$cart->id).'/edit'}}" class="tt-header__cart-edit"><i class="icon-pencil-circled"></i></a>
                                                            <form action="{{ url('carts/'.$cart->id) }}" method="post" style="display: inline">
                                                                @csrf
                                                                {{ method_field('delete') }}
                                                                <button style="border:none;margin-left:-10px;" type="submit" class="tt-header__cart-delete" title="remove"><i class="icon-trash"></i></button>
                                                            </form>
                                                        </div>

                                                    </li>





                                                @endif
                                            @endforeach
                                        @endforeach


                                    </ul>
                                    <div class="tt-header__cart-footer">
                                        <span class="tt-header__cart-subtotal colorize-head-c">Subtotal:
                                            <span class="colorize-theme-c">BDT {{ number_format($sum,2,'.',',').'/-' }}</span>
                                        </span>
                                        <div class="row">
                                            <div class="col-lg-6">
                                                <a href="{{url('/carts')}}" class="tt-header__cart-viewcart btn"><i class="icon-shop24"></i> View Cart</a>
                                            </div>
                                            <div class="col-lg-6">
                                                <a href="{{url('placeorder/create')}}" class="tt-header__cart-checkout btn colorize-btn2"><i class="icon-check"></i> Checkout</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</header>

<!-- content -->
@yield('content')
{{--content--}}

{{--catagory--}}
@yield('catagory')
{{--catagory--}}

{{--shipping_info--}}
@yield('shipping_info')
{{--shipping_info--}}

<!-- FOOTER -->
<footer>
    <div class="tt-footer tt-footer__01 ">
        <div class="tt-footer__content">
            <div class="container">
                <div class="row">
                    <div class="col-md-2">

                    </div>
                    <div class="col-lg-6">
                        <div class="tt-footer__list-menu">
                            <div class="row">
                                <div class="col-sm-4">
                                    <ul>

                                        @foreach($pages as $singlepage)
                                            <li><a href="{{url('pages/'.$singlepage->id)}}">{{$singlepage->page_title}}</a></li>

                                            @endforeach
                                    </ul>
                                </div>
                                <div class="col-sm-4">
                                    <ul>

                                    </ul>
                                </div>
                                <div class="col-sm-4">
                                    <ul>

                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <span class="tt-footer__title">Newsletter Signup</span>
                        <div class="tt-footer__newsletter">
                            <p>Sign up for our e-mail and be the first who know our special offers! Furthermore, we will
                                give a <span>15% discount</span> on the next order after you sign up.</p>
                            <form action="{{url('/subscribe')}}" method="post" class="tt-newsletter tt-newsletter--style-01">
                               {{csrf_field()}}
                                <input type="email" name="email" class="form-control"
                                       placeholder="Enter please your e-mail">
                                <input type="submit" value="Subscribe" class="btn">

                                    <i class="tt-newsletter__text-wait"></i>
                                    <span class="tt-newsletter__text-default"></span>
                                    <span class="tt-newsletter__text-error"><i class="icon-exclamation"></i>Please provide a valid email address!</span>
                                    <span class="tt-newsletter__text-complete"><i class="icon-ok"></i>Check your inbox</span>

                            </form>


                        </div>
                        <div class="tt-footer__social">
                            <div class="tt-social-icons tt-social-icons--style-01">
                                <a href="#" class="tt-btn">
                                    <i class="icon-facebook"></i>
                                </a>
                                <a href="#" class="tt-btn">
                                    <i class="icon-gplus"></i>
                                </a>
                                <a href="#" class="tt-btn">
                                    <i class="icon-twitter"></i>
                                </a>
                                <a href="#" class="tt-btn">
                                    <i class="icon-skype"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-12">
                        <span class="tt-footer__copyright">&copy; 2017 . All Rights Reserved.</span>
                    </div>
                </div>
            </div>
        </div>
        <a href="#" class="tt-footer__to-top tt-footer__to-top-desktop">
            <i class="icon-up-open-1"></i>
            <i class="icon-up"></i><span>Top</span>
        </a>
    </div>
</footer>
{{--@push('script')--}}

{{--@endpush--}}

<!-- JAVA SCRIPT -->
<!--plugins-->

<script src="{{ asset('vendor/scrollSmooth/SmoothScroll.js') }}"></script>
<script src="{{ asset('vendor/jquery/jquery.min.js') }}"></script>
<script src="{{ asset('vendor/cookie/jquery.cookie.js') }}"></script>
<script src="{{ asset('vendor/jquery/ui/jquery-ui.min.js') }}"></script>
<script src="{{ asset('vendor/velocity/velocity.min.js') }}"></script>
<script src="{{ asset('vendor/modernizr/modernizr.js') }}"></script>
<script src="{{ asset('vendor/lazyLoad/jquery.lazy.min.js') }}"></script>
<script src="{{ asset('vendor/lazyLoad/jquery.lazy.plugins.min.js') }}"></script>
<script src="{{ asset('vendor/tonymenu/js/tonymenu.js') }}"></script>
<script src="{{ asset('vendor/perfectScrollbar/js/perfect-scrollbar.jquery.min.js') }}"></script>
<script src="{{ asset('vendor/revolution/js/jquery.themepunch.tools.min.js') }}"></script>
<script src="{{ asset('vendor/revolution/js/jquery.themepunch.revolution.min.js') }}"></script>
<script src="{{ asset('vendor/countdown/jquery.countdown.min.js') }}"></script>
<script src="{{ asset('vendor/moment/moment.js') }}"></script>
<script src="{{ asset('vendor/moment/moment-timezone.js') }}"></script>
<script src="{{ asset('vendor/slick/slick.min.js') }}"></script>
<script src="{{ asset('vendor/magnificPopup/dist/jquery.magnific-popup.min.js') }}"></script>
<script src="{{ asset('vendor/elevateZoom/jquery.elevateZoom-3.0.8.min.js') }}"></script>
<script src="{{ asset('vendor/stickyBlock/sticky-sidebar.min.js') }}"></script>
<script src="{{ asset('vendor/rangeSlider/js/ion.rangeSlider.min.js') }}"></script>
<script src="{{ asset('vendor/instafeed/instafeed.min.js') }}"></script>
<script src="{{ asset('vendor/imagesLoaded/imagesloaded.pkgd.min.js') }}"></script>
<script src="{{ asset('vendor/jquery/jquery-bridget.js') }}"></script>
<script src="{{ asset('vendor/masonry/masonry.pkgd.min.js') }}"></script>
<script src="{{ asset('vendor/swiper/js/swiper.min.js') }}"></script>
<script src="{{ asset('vendor/fotorama/fotorama.js') }}"></script>


<!--modules-->
@stack('script')
<script src="{{ asset('js/app.js') }}"></script>

</body>
</html>
