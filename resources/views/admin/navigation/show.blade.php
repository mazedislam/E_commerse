@extends('admin/admin_layout')
@section('page')

    <div class="main">
        <!-- MAIN CONTENT -->
        <div class="main-content">
            <div class="container-fluid">
                <div class="col-md-12">
                    <!-- TABLE HOVER -->
                    <div class="panel">
                        <div class="panel-heading">
                            <h3 class="panel-title"> <a class='btn btn-outline-info' href="{{ url('/catagory/create')}}"></a></h3>
                        </div>
                        <div class="panel-body">

                            <table class="table">
                                <tr>
                                    <th>Navigation</th>

                                </tr>
                                <tr>
                                    <td>{{$show->title}}</td>
                                </tr>

                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection()

