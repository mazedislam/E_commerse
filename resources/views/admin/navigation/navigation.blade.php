@extends('admin/admin_layout')
@section('page')

    <div class="main">
        <!-- MAIN CONTENT -->
        <div class="main-content">
            <div class="container-fluid">

                <div class="col-md-12">
                    <!-- TABLE HOVER -->
                    <div class="panel">
                        <div class="panel-heading">
                            <h3 class="panel-title text-center">Navigation Create</h3>
                        </div>
                        <div class="panel-body">
                            <form action="{{ url('/navigation')}}@yield('id')" method="post" role="form">
                                {{csrf_field()}}
                                @yield('put_method')
                                <div class="form-group">
                                    <label for="page_title">page_title</label>
                                    <input type="text" class="form-control" value="@yield('title')" name="title" id="page_title" placeholder>
                                </div>
                                <button type="submit" class="btn btn-success">Create</button>
                            </form>

                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>

@endsection()