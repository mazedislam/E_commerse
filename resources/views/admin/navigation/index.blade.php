@extends('admin/admin_layout')
    @section('page')

        <div class="main">
            <!-- MAIN CONTENT -->
            <div class="main-content">
                <div class="container-fluid">
                    <div class="col-md-12">
                        <!-- TABLE HOVER -->
                        <div class="panel">
                            <div class="panel-heading">
                                @if(session('message'))
                                    <h3 class="alert alert-success text-center" id="message">{{ session('message') }}</h3>
                                @endif
                            </div>
                            <div class="panel-body">
                                <table class="table">
                                    <tr>
                                        <th>id</th>
                                        <th>Title</th>
                                        <th colspan="3">Action</th>
                                    </tr>

                                    @foreach($navigations as $navigation)
                                        <tr>
                                            <td>{{$navigation->id}}</td>
                                            <td>{{$navigation->title}}</td>
                                            <td><a class="btn btn-outline-info" href="{{ url('/navigation/'.$navigation->id) }}">show</a></td>
                                            <td><a class="btn btn-outline-success" href="{{ url('/navigation/'.$navigation->id.'/edit') }}">edit</a></td>
                                            <td>
                                                <form action="{{url('/navigation/'.$navigation->id)}}" method="post">
                                                    {{csrf_field()}}
                                                    {{method_field('DELETE')}}
                                                    <button class="btn btn-outline-danger" type="submit" onclick="confirm('Are You Sure Want To Delete ?')">Delete</button>
                                                </form>
                                            </td>
                                        </tr>
                                    @endforeach
                                </table>

                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>

@endsection()
@push('script')
    <script>


        jQuery(

            function($) {
                $('#message').fadeOut (550);
                $('#message').fadeIn (550);
                $('#message').fadeOut (550);
                $('#message').fadeIn (550);
                $('#message').fadeOut (550);

            }
        )
    </script>
@endpush