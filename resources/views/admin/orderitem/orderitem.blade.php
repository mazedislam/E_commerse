@extends('admin/admin_layout')
@section('page')

    <div class="main">
        <!-- MAIN CONTENT -->
        <div class="main-content">
            <div class="container-fluid">
                <div class="col-md-12">
                    <!-- TABLE HOVER -->
                    <div class="panel">
                        <div class="panel-heading">
                            <h3 class="panel-title">Order Item</h3>
                        </div>
                        <div class="panel-body">

                            <table class="table">
                                <tr>
                                    <th>order_id</th>
                                    <th>Picture</th>
                                    <th>Product_Id</th>
                                    <th>Product_Quantity</th>
                                    <th>Price</th>
                                    <th>Total Price</th>

                                </tr>
                                <?php $subtotal=0;

                                ?>

                                @foreach($orderItems as $orderitem)
                                    @foreach($Products as $product)
                                        @if($orderitem->product_id==$product->id)
                                            <?php $total=$orderitem->product_quantity*$product->price;
                                            $subtotal+=$total;
                                            ?>

                                            <tr>
                                                <td>{{$orderitem->order_id}}</td>
                                                <td><img src="{{asset('product_image/'.$product->picture)}}" style="width:100px; height:70px;"/></td>
                                                <td>{{$orderitem->product_id}}</td>
                                                <td>{{$orderitem->product_quantity}}</td>
                                                <td>{{$orderitem->product_price}}</td>
                                                <td>{{$total}}</td>
                                            </tr>

                                        @endif
                                    @endforeach
                                @endforeach
                                <tr>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td><b>SubTotal BDT {{number_format($subtotal,2,'.',',').'/-'}}</b></td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>



            </div>
        </div>
    </div>

@endsection()
