@extends('admin/admin_layout')
@section('page')

    <div class="main">
        <!-- MAIN CONTENT -->
        <div class="main-content">
            <div class="container-fluid">
                <div class="col-md-12">
                    <!-- TABLE HOVER -->
                    <div class="panel">
                        <div class="panel-heading">
                            <h3 class="panel-title text-center">Staticblock Create</h3>
                        </div>
                        <div class="panel-body">
                            <form action="{{ url('/staticblock')}}@yield('edit_id')" method="post" role="form">
                                {{csrf_field()}}
                                @yield('put_method')

                                <div class="form-group">
                                    <label for="title">Title</label>
                                    <input type="text" class="form-control" value="@yield('title')" name="title" id="title" placeholder>
                                </div>
                                <div class="form-group">
                                    <label for="content">Content</label>
                                    <textarea cols="4" rows="6" class="form-control" name="body" id="content" placeholder> @yield('body')</textarea>
                                </div>

                                <div class="form-group">
                                    <button type="submit" class="btn btn-success">Add</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>


@endsection
