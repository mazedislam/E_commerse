@extends('admin/admin_layout')
@section('page')

    <div class="main">
        <!-- MAIN CONTENT -->
        <div class="main-content">
            <div class="container-fluid">

                <a href="{{ url('/staticblock') }}" class="btn btn-primary">List</a>
                <table class="table table-bordered">

                    <tr><td>{{$show->id}}</td></tr>
                    <tr><td>Title:-2{{$show->title}}</td></tr>
                    <tr><td>Body:-{{$show->body}}</td></tr>
                    <tr><td>Created_at:-{{$show->created_at->diffForHumans()}}</td></tr>

                </table>

            </div>
        </div>
    </div>

@endsection()

