@extends('admin/admin_layout')
@section('page')

    <div class="main">
        <!-- MAIN CONTENT -->
        <div class="main-content">
            <div class="container-fluid">
                <div class="col-md-12">
                    <!-- TABLE HOVER -->
                    <div class="panel">
                        <div class="panel-heading">
                            <h3 class="panel-title text-center">Staticblock list</h3>
                        </div>
                        <div class="panel-body">
                            <table class="table table-hover table-dark">
                                <tr>
                                    <th>Title</th>
                                    <th>Content</th>
                                    <th>show</th>
                                    <th>view</th>
                                    <th>Delete</th>
                                </tr>

                                @foreach($staticblocks as $staticblock)
                                    <tr>
                                        <td>{{$staticblock->title}}</td>
                                        <td>{{$staticblock->body}}</td>
                                        <td><a class="btn btn-outline-info" href="{{ url('/staticblock/'.$staticblock->id) }}">show</a></td>
                                        <td><a class="btn btn-outline-success" href="{{ url('/staticblock/'.$staticblock->id.'/edit') }}">edit</a></td>
                                        <td>
                                            <form action="{{url('/staticblock/'.$staticblock->id)}}" method="post">
                                                {{csrf_field()}}
                                                {{method_field('DELETE')}}
                                                <button class="btn btn-outline-danger" type="submit" onclick="confirm('Are You Sure Want To Delete ?')">Delete</button>
                                            </form>
                                        </td>
                                    </tr>
                                @endforeach
                            </table>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>

@endsection()
