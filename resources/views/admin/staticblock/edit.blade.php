@extends('admin/admin_layout')
@section('page')

    <div class="main">
        <!-- MAIN CONTENT -->
        <div class="main-content">
            <div class="container-fluid">
                <div class="col-md-12">
                    <!-- TABLE HOVER -->
                    <div class="panel">
                        <div class="panel-heading">
                            <h3 class="panel-title text-center">Staticblock Edit</h3>
                        </div>
                        <div class="panel-body">
                            <form action="{{ url('/staticblock/'.$edit->id)}}" method="post" role="form">
                                {{csrf_field()}}
                                {{method_field('PUT')}}

                                <div class="form-group">
                                    <label for="title">Title</label>
                                    <input type="text" class="form-control" value="{{$edit->title}}" name="title" id="title" placeholder>
                                </div>
                                <div class="form-group">
                                    <label for="content">Content</label>
                                    <textarea cols="4" rows="6" class="form-control" name="body" id="content" placeholder>{{$edit->body}}</textarea>
                                </div>

                                <div class="form-group">
                                    <button type="submit" class="btn btn-success">Update</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>


@endsection

