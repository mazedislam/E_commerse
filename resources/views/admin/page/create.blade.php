@extends('admin/admin_layout')
@section('page')

    <div class="main">
        <!-- MAIN CONTENT -->
        <div class="main-content">
            <div class="container-fluid">
                <div class="col-md-12">
                    <!-- TABLE HOVER -->
                    <div class="panel">
                        <div class="panel-heading">
                            <h3 class="panel-title text-center">Page Create</h3>
                        </div>
                        <div class="panel-body">
                            <form action="{{ url('/pages')}}" method="post" role="form">
                                {{csrf_field()}}
                                <div class="form-group">
                                    <label for="page_title"></label>
                                    <input type="text" class="form-control" value="" name="page_title" id="page_title" placeholder>
                                </div>
                                <div class="form-group">
                                    <label for="page_content">Content</label>
                                    <textarea cols="6" rows="4" class="form-control" name="page_content" id="page_content" placeholder></textarea>
                                </div>

                                <button type="submit" class="btn btn-success">Submit</button>
                            </form>

                        </div>
                    </div>
                </div>


            </div>


            </div>
        </div>

@endsection()
@push('script')
    <script>


        jQuery(

            function($) {
                $('#message').fadeOut (550);
                $('#message').fadeIn (550);
                $('#message').fadeOut (550);
                $('#message').fadeIn (550);
                $('#message').fadeOut (550);
                $('#message').fadeIn (550);
                $('#message').fadeOut (550);
            }
        )
    </script>
    @endpush



