@extends('admin/admin_layout')
@section('page')

    <div class="main">
        <!-- MAIN CONTENT -->
        <div class="main-content">
            <div class="container-fluid">
                <div class="col-md-12">
                    <!-- TABLE HOVER -->
                    <div class="panel">
                        <div class="panel-heading">
                            <h3 class="panel-title text-center">Page Update</h3>
                        </div>
                        <div class="panel-body">
                            <form action="{{ url('/pages/'.$edit->id)}}" method="post" role="form">
                                {{csrf_field()}}
                                {{method_field('PUT')}}
                                <div class="form-group">
                                    <label for="page_title"></label>
                                    <input type="text" class="form-control" value="{{$edit->page_title}}" name="page_title" id="page_title" placeholder>
                                </div>
                                <div class="form-group">
                                    <label for="page_content">Content</label>
                                    <textarea cols="6" rows="4" class="form-control"  name="page_content" id="page_content" placeholder>{{$edit->page_content}}</textarea>
                                </div>

                                <button type="submit" class="btn btn-success">Update</button>
                            </form>

                        </div>
                    </div>
                </div>


            </div>


        </div>
    </div>
@endsection()


