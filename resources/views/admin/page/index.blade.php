@extends('admin/admin_layout')
@section('page')

    <div class="main">
        <!-- MAIN CONTENT -->
        <div class="main-content">
            <div class="container-fluid">
                <div class="col-md-12">
                    <!-- TABLE HOVER -->
                    <div class="panel">
                        <div class="panel-heading">
                            @if(session('message'))
                                <h3 class="alert alert-success text-center" id="message">{{ session('message') }}</h3>
                            @endif
                        </div>
                        <div class="panel-body">
                            <table class="table">
                                <tr>
                                    <th>Sl No</th>
                                    <th>page title</th>
                                    <th>page content</th>
                                    <th colspan="3">Action</th>
                                </tr>
                                @foreach($pages as $page)
                                    <tr>
                                        <td>{{$loop->index+1}}</td>
                                        <td>{{$page->page_title}}</td>
                                        <td>{{$page->page_content}}</td>
                                        <td><a class="btn btn-outline-info" href="{{ url('/pages/'.$page->id) }}">show</a></td>
                                        <td><a class="btn btn-outline-success" href="{{ url('/pages/'.$page->id.'/edit') }}">edit</a></td>
                                        <td>
                                            <form action="{{url('/pages/'.$page->id)}}" method="post">
                                                {{csrf_field()}}
                                                {{method_field('DELETE')}}
                                                <button class="btn btn-outline-danger" type="submit" onclick="confirm('Are You Sure Want To Delete ?')">Delete</button>
                                            </form>
                                        </td>
                                    </tr>

                                @endforeach
                            </table>

                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>

@endsection()
@push('script')
    <script>


        jQuery(

            function($) {
                $('#message').fadeOut (550);
                $('#message').fadeIn (550);
                $('#message').fadeOut (550);
                $('#message').fadeIn (550);
                $('#message').fadeOut (550);

            }
        )
    </script>
@endpush


