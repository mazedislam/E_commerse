@extends('layouts/layout')
@section('title',$show->page_title)
@section('content')

    <div class="container">
        <!-- MAIN CONTENT -->
        <div class="panel-heading">
            <h3 class="panel-title text-center">{{$show->page_title}}</h3>
        </div>
        <div class="panel-body">
            <?php echo $show->page_content?>
        </div>
    </div>

@endsection()

