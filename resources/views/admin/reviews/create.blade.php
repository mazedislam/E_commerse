<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>reviews</title>
    <link rel="stylesheet" href="{{ asset('css/app.css') }}">
</head>
<body>

<div class="container">
    <a href="{{ url('/sliders') }}" class="btn btn-primary">List</a>

    <form action="{{ url('/reviews') }}@yield('edit_id')" method="post">
        @csrf
        @yield('PUT')
        @show

        <div class="form-group">
            <input type="number" class="form-control" value="@yield('product_id')" name="product_id" placeholder="product_id">
        </div>
        <div class="form-group">
            <input type="text" class="form-control" value="@yield('name')"  name="name" placeholder="name">
        </div>
        <div class="form-group">
            <input type="email" class="form-control" value="@yield('Email')" name="Email" placeholder="email">
        </div>
        <div class="form-group">
            <input type="date" value="@yield('start_date')" class="form-control" name="start_date">
        </div>
        <div class="form-group">
            <input type="text" class="form-control" value="@yield('review_title')" name="review_title" placeholder="review">
        </div>
        <div class="form-group">
            <textarea class="form-control" name="body_review" placeholder="body">@yield('body_review')</textarea>
        </div>

        <div class="form-group">
            <button type="submit" class="btn btn-success">Add</button>
        </div>
    </form>
</div>

</body>
</html>