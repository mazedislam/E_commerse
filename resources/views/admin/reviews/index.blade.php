@extends('admin/admin_layout')
@section('page')

    <div class="main">
        <!-- MAIN CONTENT -->
        <div class="main-content">
            <div class="container-fluid">

                @if(session('message'))
                    <span class="alert alert-success">{{ session('message') }}</span>
                @endif

                    <div class="col-md-12">
                        <!-- TABLE HOVER -->
                        <div class="panel">
                            <div class="panel-heading">
                                @if(session('message'))
                                    <h3 class="alert alert-success text-center" id="message">{{ session('message') }}</h3>
                                @endif
                            </div>
                            <div class="panel-body">
                                <table class="table table-hover table-dark">
                                    <thead>
                                    <tr>
                                        <th>product_id</th>
                                        <th>name</th>
                                        <th>Email</th>
                                        <th>date</th>
                                        <th>review_title</th>
                                        <th>body_review</th>
                                        <th colspan="2">Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($reviews as $review)
                                        <tr>
                                            <td>{{ $review->product_id }}</td>
                                            <td>{{ $review->name }}</td>
                                            <td>{{ $review->Email }}</td>
                                            <td>{{ $review->start_date }}</td>
                                            <td>{{ $review->review_title }}</td>
                                            <td>{{ $review->body_review }}</td>
                                            <td><a class='btn btn-outline-primary' href="{{ url('reviews/'.$review->id) }}">Show</a></td>
                                            <td><form action="{{ url('reviews/'.$review->id) }}" method="post" style="display: inline">
                                                    @csrf
                                                    {{ method_field('delete') }}
                                                    <button type="submit" class="btn btn-outline-danger" onclick="return confirm('Are You Sure Want To Delete ?')">Delete</button>
                                                </form>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>

                            </div>
                        </div>
                    </div>
            </div>
        </div>
    </div>

@endsection()

