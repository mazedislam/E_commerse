@extends('admin/admin_layout')
@section('page')

    <div class="main">
        <!-- MAIN CONTENT -->
        <div class="main-content">
            <div class="container-fluid">
                <div class="col-md-12">
                    <!-- TABLE HOVER -->
                    <div class="panel">
                        <div class="panel-heading">
                            <h3 class="panel-title text-center">Product Create</h3>
                        </div>
                        <div class="panel-body">

                            <form action="{{ url('/products') }}" method="post" enctype="multipart/form-data">
                                @csrf
                                @yield('method_field')
                                <div class="form-group">
                                    <select id="" name="catagory_id" class="form-control">
                                        <option value="">Select category</option>
                                        @foreach($categories as $key=>$category)
                                            <option value="{{$key}}">{{$category}}</option>
                                        @endforeach

                                    </select>
                                </div>
                                <div class="form-group">
                                    <input type="text" class="form-control" name="title"  placeholder="title">
                                </div>
                                <div class="form-group">
                                    <input type="file" class="form-control-file"  name="picture">
                                </div>
                                @yield('picture')
                                <div class="form-group">

                                    <textarea rows="2" class="form-control" cols="6" name="short_description" placeholder="short_description"></textarea>
                                </div>
                                <div class="form-group">

                                    <textarea rows="6" class="form-control" cols="6" name="description" placeholder="description"></textarea>
                                </div>
                                <div class="form-group">
                                    <textarea class="form-control"  rows="6" cols="6" name="additional_information" placeholder="additional_information"></textarea>
                                </div>
                                <div class="form-group">
                                    <input type="number" class="form-control"  name="price" placeholder="price">
                                </div>
                                <div class="form-group">
                                    <input type="number" class="form-control" name="special_price" placeholder="special_price">
                                </div>

                                <div class="form-group">

                                    <label class="radio-inline">
                                        <input type="radio" name="offer" value="offer "> Offer
                                    </label>
                                    <label class="radio-inline">
                                        <input type="radio" name="offer" value="No_Offer" checked>  No_Offer
                                    </label>

                                </div>

                                <div class="form-group">
                                    <input type="date" class="form-control"  name="start_date" placeholder="start_date">
                                </div>
                                <div class="form-group">
                                    <input type="date" class="form-control"  name="end_date" placeholder="end_date">
                                </div>
                                <div class="form-group">
                                    <input type="text" class="form-control"   name="sku" placeholder="sku">
                                </div>
                                <div class="form-group">
                                    <input type="text" class="form-control"  name="meta_keyword" placeholder="meta_keyword">
                                </div>
                                <div class="form-group">
                                    <input type="text" class="form-control" name="meta_description" placeholder="meta_description">
                                </div>
                                <div class="form-group">
                                    <input type="text" class="form-control"   name="product_url" placeholder="product_url">
                                </div>
                                <div class="form-group">
                                    <button type="submit" class="btn btn-success">Add</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>

@endsection()

