@extends('admin/admin_layout')
@section('page')

    <div class="main">
        <!-- MAIN CONTENT -->
        <div class="main-content">
            <div class="container-fluid">
                @if(session('message'))
                    {{--<h3 class="alert alert-success text-center" id="message">{{ session('message') }}</h3>--}}
                @endif

                    <a class='btn btn-outline-primary float-right' href="{{ url('products/create')}}">Add</a>
                <table class="table">
                    <thead>
                    <tr>
                        <th>title</th>
                        <th>picture</th>
                        <th>short_description</th>
                        <th>description</th>
                        {{--   <th>addtional_information</th>--}}
                        <th>price</th>
                        <th>special_price</th>
                        <th>start_date</th>
                        <th>end_date</th>
                        {{--<th>sku</th>
                        <th>meta_keyword</th>
                        <th>meta_description</th>
                        <th>product_url</th>--}}
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($products as $product)
                        <tr>
                            <td>{{$product->title}}</td>
                            <td><img  src="{{asset('product_image/'.$product->picture)}}" width="200px"></td>
                            <td>{{$product->short_description}}</td>
                            <td>{{$product->description}}</td>
                            <td>{{$product->additional_information}}</td>
                            <td>{!! $product->price !!}</td>
                            <td>{{$product->special_price}}</td>
                            <td>{{$product->start_date}}</td>
                            <td>{{$product->end_date}}</td>
                            {{--<td>{{$product->sku}}</td>
                            <td>{{$product->meta_keyword}}</td>
                            <td>{{$product->meta_description}}</td>
                            <td>{{$product->product_url}}</td>--}}
                           {{-- <td><a class='btn btn-outline-primary' href="{{ url('products/'.$product->id) }}">Show</a></td>--}}
                            <td> <a class="btn btn-outline-success" href="{{ url('products/'.$product->id.'/edit') }}"> Edit </a></td>
                            <td><form action="{{ url('products/'.$product->id) }}" method="post" style="display: inline">
                                    @csrf
                                    {{ method_field('delete') }}
                                    <button type="submit" class="btn btn-outline-danger" onclick="return confirm('Are You Sure Want To Delete ?')">Delete</button>
                                </form>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>

            </div>
        </div>
    </div>

@endsection()
@push('script')
    <script>


        jQuery(

            function($) {
                $('#message').fadeOut (550);
                $('#message').fadeIn (550);
                $('#message').fadeOut (550);
                $('#message').fadeIn (550);
                $('#message').fadeOut (550);

            }
        )
    </script>
@endpush