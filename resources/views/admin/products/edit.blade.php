<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" href="{{ asset('css/app.css') }}">
</head>
<body>
<div class="main">
    <!-- MAIN CONTENT -->
    <div class="main-content">
        <div class="container-fluid">
            <div class="col-md-12">
                <!-- TABLE HOVER -->
                <div class="panel">
                    <div class="panel-heading">
                        <h3 class="panel-title text-center">Product edit</h3>
                    </div>
                    <div class="panel-body">
                        <form action="{{url('/products/'.$edit->id )}}" method="post" enctype="multipart/form-data">
                            @csrf
                            {{method_field('PUT')}}
                            <div class="form-group ">
                                <select id="" name="catagory_id" class="form-control">
                                    <option value="">Select category</option>
                                    @foreach($categories as $key=>$category)
                                        <option value="{{$key}}" @if($edit->id==$key){{'selected'}}@endif>{{$category}}</option>
                                    @endforeach

                                </select>
                            </div>
                            <div class="form-group">
                                <input type="text" class="form-control" value="{{$edit->title}}"  name="title"  placeholder="title">
                            </div>
                            <div class="form-group">
                                <input type="file" class="form-control-file"  name="picture">
                                <img src="{{ asset('product_image/'.$edit->picture) }}" width="150">
                            </div>


                            <div class="form-group">

                                <textarea rows="6" class="form-control" cols="6" name="short_description" placeholder="short_description">{{$edit->short_description}}</textarea>
                            </div>
                            <div class="form-group">

                                <textarea rows="6" class="form-control" cols="6" name="description" placeholder="description">{{$edit->description}}</textarea>
                            </div>

                            <div class="form-group">
                                <textarea rows="6" class="form-control" cols="6"  name="additional_information" placeholder="additional_information">{{$edit->additional_information}}</textarea>
                            </div>
                            <div class="form-group">
                                <input type="number" class="form-control" value="{{$edit->price}}" name="price" placeholder="price">
                            </div>
                            <div class="form-group">
                                <input type="number" class="form-control" value="{{$edit->special_price}}" name="special_price" placeholder="special_price">
                            </div>

                            <div class="form-group">

                                <label class="radio-inline">
                                    <input type="radio" name="offer" value="offer" @if($edit->offer=='offer') checked @endif> Offer
                                </label>
                                <label class="radio-inline">
                                    <input type="radio" name="offer" value="No_Offer "  @if($edit->offer=='No_Offer') checked @endif>  No_Offer
                                </label>

                            </div>

                            <div class="form-group">
                                <input type="date" class="form-control" value="{{$edit->start_date}}"  name="start_date" placeholder="start_date">
                            </div>
                            <div class="form-group">
                                <input type="date" class="form-control" value="{{$edit->end_date}}"  name="end_date" placeholder="end_date">
                            </div>
                            <div class="form-group">
                                <input type="text" class="form-control" value="{{$edit->sku}}"  name="sku" placeholder="sku">
                            </div>
                            <div class="form-group">
                                <input type="text" class="form-control" value="{{$edit->meta_keyword}}"  name="meta_keyword" placeholder="meta_keyword">
                            </div>
                            <div class="form-group">
                                <input type="text" class="form-control" value="{{$edit->meta_description}}"  name="meta_description" placeholder="meta_description">
                            </div>
                            <div class="form-group">
                                <input type="text" class="form-control" value="{{$edit->product_url}}"  name="product_url" placeholder="product_url">
                            </div>
                            <div class="form-group">
                                <button type="submit" class="btn btn-success">Update</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>
</body>
</html>