<!doctype html>
<html lang="en">

<head>
    <title>ShopHub | Admin</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <!-- VENDOR CSS -->
    <link rel="stylesheet" href="{{asset('assets/vendor/bootstrap/css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('assets/vendor/font-awesome/css/font-awesome.min.css')}}">
    <link rel="stylesheet" href="{{asset('assets/vendor/linearicons/style.css')}}">
    <link rel="stylesheet" href="{{asset('assets/vendor/chartist/css/chartist-custom.css')}}">
    <!-- MAIN CSS -->
    <link rel="stylesheet" href="{{asset('assets/css/main.css')}}">
    <!-- FOR DEMO PURPOSES ONLY. You should remove this in your project -->
    <link rel="stylesheet" href="{{asset('assets/css/demo.css')}}">
    <!-- GOOGLE FONTS -->
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700" rel="stylesheet">
    <!-- ICONS -->
    <link rel="apple-touch-icon" sizes="76x76" href="{{asset('favicon.png')}}">
 {{--   <link rel="icon" type="image/png" sizes="96x96" href="{{asset('favicon.png')}}">--}}
</head>

<body>
<!-- WRAPPER -->
<div id="wrapper">
    <!-- NAVBAR -->
    <nav class="navbar navbar-default navbar-fixed-top">
        <div class="container-fluid">
            <div class="navbar-btn">
                <button type="button" class="btn-toggle-fullwidth"><i class="lnr lnr-arrow-left-circle"></i></button>
            </div>

            <div id="navbar-menu">
                <ul class="nav navbar-nav navbar-right">


                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown"><img src="../../../public/assets/img/user.png" class="img-circle" alt=""> <span>{{ Auth::user()->name }}</span> <i class="icon-submenu lnr lnr-chevron-down"></i></a>
                        <ul class="dropdown-menu">
                            <li><a href="#"><i class="lnr lnr-user"></i> <span>My Profile</span></a></li>
                            <li><a href="#"><i class="lnr lnr-envelope"></i> <span>Message</span></a></li>
                            <li><a href="#"><i class="lnr lnr-cog"></i> <span>Settings</span></a></li>
                            <li>
                                <a class="" href="{{ route('logout') }}"
                                   onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                    <i class="lnr lnr-exit"></i> <span>Logout</span>
                                </a>

                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                    @csrf
                                </form>


                        </ul>
                    </li>

                </ul>
            </div>
        </div>
    </nav>

    <div id="sidebar-nav" class="sidebar">
        <div class="sidebar-scroll">
            <nav>
                <ul class="nav">

                    <li>

                        <a href="#navigation" data-toggle="collapse" class="collapsed"><i class="fa fa-navicon"></i> <span>Navigation</span> <i class="icon-submenu lnr lnr-chevron-left"></i></a>
                        <div id="navigation" class="collapse ">
                            <ul class="nav">
                                <li><a href="{{url('navigation/create')}}" class=""><i class="fa fa-plus-square"></i> Add</a></li>
                                <li><a href="{{url('/navigation')}}" class=""><i class="fa fa-list-ul"></i> index</a></li>
                            </ul>
                        </div>

                    </li>
                    <li>

                        <a href="#brand" data-toggle="collapse" class="collapsed"><i class="fa fa-bandcamp"></i> <span>Category</span> <i class="icon-submenu lnr lnr-chevron-left"></i></a>
                        <div id="brand" class="collapse ">
                            <ul class="nav">
                                <li><a href="{{url('catagory/create')}}" class=""><i class="fa fa-plus-square"></i> Add</a></li>
                                <li><a href="{{url('catagory')}}" class=""><i class="fa fa-list-ul"></i> index</a></li>
                            </ul>
                        </div>

                    </li>
                    <li>

                        <a href="#product" data-toggle="collapse" class="collapsed"><i class="fa fa-product-hunt"></i> <span>Products</span> <i class="icon-submenu lnr lnr-chevron-left"></i></a>
                        <div id="product" class="collapse ">
                            <ul class="nav">
                                <li><a href="{{url('products/create')}}" class=""><i class="fa fa-plus-square"></i> Add</a></li>
                                <li><a href="{{url('products')}}" class=""><i class="fa fa-list-ul"></i> index</a></li>
                            </ul>
                        </div>

                    </li>
                    <li>

                        <a href="#placeorder" data-toggle="collapse" class="collapsed"><i class="fa fa-shopping-cart"></i> <span>Placeorder</span> <i class="icon-submenu lnr lnr-chevron-left"></i></a>
                        <div id="placeorder" class="collapse ">
                            <ul class="nav">
                                <li><a href="{{url('placeorder/create')}}" class=""><i class="fa fa-plus-square"></i> Add</a></li>
                                <li><a href="{{url('placeorder')}}" class=""><i class="fa fa-list-ul"></i> index</a></li>
                            </ul>
                        </div>

                    </li>

                    <li>

                        <a href="#review" data-toggle="collapse" class="collapsed"><i class="fa fa-comment"></i> <span>Review</span> <i class="icon-submenu lnr lnr-chevron-left"></i></a>
                        <div id="review" class="collapse ">
                            <ul class="nav">
                                <li><a href="{{url('reviews')}}" class=""><i class="fa fa-list-ul"></i> index</a></li>
                            </ul>
                        </div>

                    </li>
                    <li>

                        <a href="#slider" data-toggle="collapse" class="collapsed"><i class="fa fa-sliders"></i> <span>Slider</span> <i class="icon-submenu lnr lnr-chevron-left"></i></a>
                        <div id="slider" class="collapse ">
                            <ul class="nav">
                                <li><a href="{{url('sliders/create')}}" class=""><i class="fa fa-plus-square"></i> Add</a></li>
                                <li><a href="{{url('sliders')}}" class=""><i class="fa fa-list-ul"></i> index</a></li>

                            </ul>
                        </div>

                    </li>
                    <li>

                        <a href="#staticblock" data-toggle="collapse" class="collapsed"><i class="fa fa-sliders"></i> <span>Staticblock</span> <i class="icon-submenu lnr lnr-chevron-left"></i></a>
                        <div id="staticblock" class="collapse ">
                            <ul class="nav">
                                <li><a href="{{url('/staticblock/create')}}" class=""><i class="fa fa-plus-square"></i> Add</a></li>
                                <li><a href="{{url('staticblock')}}" class=""><i class="fa fa-list-ul"></i> index</a></li>

                            </ul>
                        </div>

                    </li>

                    <li>
                        <a href="#Pages" data-toggle="collapse" class="collapsed"><i class="fa fa-paperclip"></i> <span>Pages</span> <i class="icon-submenu lnr lnr-chevron-left"></i></a>
                        <div id="Pages" class="collapse ">
                            <ul class="nav">
                                <li><a href="{{url('pages/create')}}" class=""><i class="fa fa-plus-square"></i> Add</a></li>
                                <li><a href="{{url('pages')}}" class=""><i class="fa fa-list-ul"></i> index</a></li>

                            </ul>
                        </div>
                    </li>
                </ul>
            </nav>
        </div>


    </div>

    @yield('page')


</div>


<!-- END WRAPPER -->
<!-- Javascript -->
<script src="{{asset('assets/vendor/bootstrap/js/jquery.js')}} "></script>
<script src="{{asset('assets/vendor/jquery/jquery.min.js')}}"></script>
<script src="{{asset('assets/vendor/bootstrap/js/bootstrap.min.js')}} "></script>
<script src="{{asset('assets/vendor/jquery-slimscroll/jquery.slimscroll.min.js')}} "></script>
<script src="{{asset('assets/vendor/chartist/js/chartist.min.js')}} "></script>
<script src="{{asset('assets/scripts/klorofil-common.js')}} "></script>

@stack('script')
<script>
    $(function() {
        var options;

        var data = {
            labels: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
            series: [
                [200, 380, 350, 320, 410, 450, 570, 400, 555, 620, 750, 900],
            ]
        };

        // line chart
        options = {
            height: "300px",
            showPoint: true,
            axisX: {
                showGrid: false
            },
            lineSmooth: false,
        };

        new Chartist.Line('#demo-line-chart', data, options);

        // bar chart
        options = {
            height: "300px",
            axisX: {
                showGrid: false
            },
        };

        new Chartist.Bar('#demo-bar-chart', data, options);


        // area chart
        options = {
            height: "270px",
            showArea: true,
            showLine: false,
            showPoint: false,
            axisX: {
                showGrid: false
            },
            lineSmooth: false,
        };

        new Chartist.Line('#demo-area-chart', data, options);


        // multiple chart
        var data = {
            labels: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
            series: [{
                name: 'series-real',
                data: [200, 380, 350, 320, 410, 450, 570, 400, 555, 620, 750, 900],
            }, {
                name: 'series-projection',
                data: [240, 350, 360, 380, 400, 450, 480, 523, 555, 600, 700, 800],
            }]
        };

        var options = {
            fullWidth: true,
            lineSmooth: false,
            height: "270px",
            low: 0,
            high: 'auto',
            series: {
                'series-projection': {
                    showArea: true,
                    showPoint: false,
                    showLine: false
                },
            },
            axisX: {
                showGrid: false,

            },
            axisY: {
                showGrid: false,
                onlyInteger: true,
                offset: 0,
            },
            chartPadding: {
                left: 20,
                right: 20
            }
        };

        new Chartist.Line('#multiple-chart', data, options);

    });
</script>
</body>

</html>
