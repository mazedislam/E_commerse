@extends('layouts/layout')
@section('content')
    <div class="container">

        <table class="table table-condensed">
            <tr>
                <td>My Orders</td>
            </tr>
            <tr>
                <td>Order #</td>
                <td>Date</td>
                <td>Ship to</td>
                <td>order status</td>
                <td colspan="2"></td>

            </tr>

                    <tr>
                        <td>{{$order->order_id}}</td>
                        <td>{{$order->created_at}}</td>
                        <td>{{$order->name}}</td>
                        <td>
                            @if($order->status==0)
                                Pending
                            @elseif($order->status==1)
                                Verified
                            @elseif($order->status==2)
                                Delivered
                            @endif

                        </td>
                        <td><a style="color:red" href="{{url('placeorder/'.$order->order_id)}}">View Order</a></td>
                        <td><a style="color:red" href="{{url('orders/'.$order->order_id)}}">Recoder</a></td>
                    </tr>
        </table>
        <a href="" style="margin-bottom:15px" class="btn btn-primary"><< Back</a>


    </div>
@endsection