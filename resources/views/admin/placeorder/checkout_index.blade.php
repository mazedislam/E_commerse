@extends('admin/admin_layout')
@section('page')

    <div class="main">
        <!-- MAIN CONTENT -->
        <div class="main-content">
            <div class="container-fluid">

                <table class="table">
                    <h1 class="text-center">Order List</h1>
                    <tr>
                        <th>order_id</th>
                        <th>OrderCreate</th>
                        <th>name</th>
                        <th>address</th>
                        <th>email</th>
                        <th>phone</th>
                        <th>paymentMethod</th>
                        <th>transactionnumber</th>
                        <th>status</th>
                        <th colspan="3">Action</th>

                    </tr>

                    @foreach($placeorders as $placeorder)
                        <tr>
                            <td>{{$placeorder->order_id}}</td>
                            <td>{{$placeorder->created_at->diffForHumans()}}</td>
                            <td>{{$placeorder->name}}</td>
                            <td>{{$placeorder->address}}</td>
                            <td>{{$placeorder->email}}</td>
                            <td>{{$placeorder->phone}}</td>
                            <td>{{$placeorder->paymentMethod}}</td>
                            <td>{{$placeorder->transactionnumber}}</td>
                            <td>
                                @if($placeorder->status==0)
                                    Pending
                                @elseif($placeorder->status==1)
                                    Verified
                                @elseif($placeorder->status==2)
                                    Delivered
                                @endif

                            </td>
                            <td><a href="{{url('orders/'.$placeorder->order_id)}}"><button>Detail</button></a></td>
                            <td>
                                <form action="{{ url('placeorder/'.$placeorder->id)}}" method="post">
                                    @csrf
                                    {{method_field('PUT')}}
                                    <input type="hidden" value="1" name="status">
                                    <button type="submit">Verify</button>
                                </form>
                            </td>
                            <td>
                                <form action="{{ url('placeorder/'.$placeorder->id)}}" method="post">
                                    @csrf
                                    {{method_field('PUT')}}
                                    <input type="hidden" value="2" name="status">
                                    <button type="submit">Delivery</button>
                                </form>
                            </td>



                        </tr>

                    @endforeach
                </table>

            </div>
        </div>
    </div>

@endsection()
