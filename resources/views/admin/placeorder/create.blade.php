@extends('layouts/layout')
@section('content')
    <main>

        <div class="tt-layout tt-sticky-block__parent ">
            <div class="tt-layout__content">
                <div class="container">
                    <div class="tt-page__breadcrumbs">
                        <ul class="tt-breadcrumbs">
                            <li><a href="{{url('home')}}"><i class="icon-home"></i></a></li>
                            <li><a href="{{url('productlist')}}">Products</a></li>
                            <li><span>Checkout</span></li>
                        </ul>
                    </div>

                    <div class="tt-page__name text-center">
                        <h1>Checkout</h1>
                    </div>

                    <div class="tt-checkout">
                        <div class="row">
                            <div class="col-lg-8">
                                <form action="{{url('/placeorder')}}" method="post">
                                    @csrf

                                <h4 class="ttg-mt--50 ttg-mb--20">Shipping Address</h4>
                                <div class="tt-checkout__form">
                                    <div class="row">

                                        <div class="col-md-2"><p></p></div>
                                        <div class="col-md-10">
                                            <div class="tt-input">
                                                <input  type="hidden" name="order_id" class="form-control colorize-theme6-bg" value="{{rand()}}" placeholder="Order Id">
                                            </div>
                                        </div>


                                        <div class="col-md-2"><p>Name:</p></div>
                                        <div class="col-md-10">
                                            <div class="tt-input">
                                                <input type="text" value="" name="name" class="form-control colorize-theme6-bg" placeholder="Example: Hasan" required>
                                            </div>
                                        </div>


                                        <div class="col-md-2"><p>Address:</p></div>
                                        <div class="col-md-10">
                                            <div class="tt-input tt-input-valid--true">

                                                <input type="text"  name="address" class="form-control colorize-theme6-bg" placeholder="Example:MuradPur,Chittagong." required>
                                            </div>
                                        </div>
                                        <div class="col-md-2"><p>Email:</p></div>
                                        <div class="col-md-10">
                                            <div class="tt-input tt-input-valid--false">
                                                <input type="text" name="email" class="form-control colorize-theme6-bg" placeholder="Example: xyz@gmail.com" required>


                                            </div>
                                        </div>
                                        <div class="col-md-2"><p>Phone:</p></div>
                                        <div class="col-md-10">
                                            <div class="tt-input">
                                                <input type="text" name="phone" class="form-control colorize-theme6-bg" placeholder="Example:  01812-345677" required>
                                            </div>
                                        </div>

                                    </div>

                                </div>
                                <h4 class="tt-checkout--border ttg-mt--50 ttg-mb--20">Payment Methods</h4>
                                <div class="tt-checkout__methods">
                                    <ul>
                                        <li>
                                            <label class="tt-checkbox-circle">
                                                <input  id="bkash" type="radio" name="paymentMethod" value="bkash" required>
                                                <span></span>
                                                <p><img src="{{asset("bkash.png")}}"></p>
                                            </label>
                                        </li>
                                        <li class="hidden">
                                            <div id="transactionNumber">
                                                Our Merchant Account Nos: 01941900900
                                                <input type="text" id="" name="transactionnumber" class=" form-control  colorize-theme6-bg" placeholder="Transition Number"><br>
                                                How to pay using bKash?

                                                <ul>
                                                    <li>Pay through bKash and confirm your transaction ID</li>
                                                    <li>Your bKash amount must be equal or greater than the ordered amount.</li>
                                                    <li>Please save your bKash Reservation Reference Number for future reference.</li>
                                                </ul>
                                            </div>


                                        </li>
                                        <li>
                                            <label class="tt-checkbox-circle cash">
                                                <input id="cashOnDelivery" type="radio" name="paymentMethod" value="cashOnDelivery" required>
                                                <span></span>
                                                <p>Cash On Delibary</p>
                                            </label>
                                        </li>



                                    </ul>
                                </div>


                                <div class="tt-checkout--border">
                                    <button type="submit" class="tt-checkout__btn-order btn btn-type--icon colorize-btn6"><i class="icon-check"></i><span>PlaceOrder</span></button>
                                </div>
                                </form>
                            </div>

                            <div class="col-lg-4">
                                <div class="tt-summary">
                                    <div class="tt-summary--border">
                                       @if(count($products)==0)
                                            <h6>Your Are No Order Product</h6>
                                        @elseif(count($products)>0)
                                            <h6>Order Summary</h6>
                                        @endif

                                    </div>
                                    <?php $sum=0;?>
                                    <div class="tt-summary__products tt-summary--border">
                                                         <ul>
                                                             @foreach($carts as $cart)
                                                                 @foreach($products as $product)
                                                                     @if($cart->product_id==$product->id)
                                                                       <li>
                                                                            <?php
                                                                            $total=$product->price*$cart->product_quantity;
                                                                            $sum=$sum+$total;
                                                                             ?>
                                                                            <div>
                                                                            <a href="#"><img src="{{asset('product_image/'.$product->picture)}}"
                                                                                 alt="Image name"></a>
                                                                            </div>
                                                                            <div>
                                                                            <p><a href="#">Elegant and fresh. A most attractive mobile...</a></p>
                                                                            <span class="tt-summary__products_price">
                                                                            <span class="tt-summary__products_price-count">{{$cart->product_quantity}}</span><span>x</span>
                                                                            <span class="tt-summary__products_price-val">
                                                                            <span class="tt-price">
                                                                            <span>{{$product->price}}</span>
                                                                            </span>
                                                                            </span>
                                                                            </span>
                                                                            <div class="tt-summary__products_param-control tt-summary__products_param-control--open">
                                                                                <span><a href="{{url('carts/'.$cart->id)}}">Details</a></span>
                                                                                <i class="icon-down-open"></i>
                                                                            </div>
                                                                            </div>
                                                                      </li>
                                                                     @endif
                                                                 @endforeach
                                                             @endforeach

                                                        </ul>
                                    </div>

                                    <script>
                                        require(['app'], function () {
                                            require(['modules/toggleProductParam']);
                                        });
                                    </script>
                                    <div class="tt-summary--border">
                                        <div class="tt-summary__total">

                                                <p>Subtotal: <span>BDT:{{ number_format($sum,2,'.',',').'/-' }}</span></p>


                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>
@endsection
@push('script')
    <script>
        $('#transactionNumber').addClass('hidden');
        $('#bkash').on('click',function(){

            $('#transactionNumber').removeClass('hidden');

        });
        $('#cashOnDelivery').on('click',function(){

            $('#transactionNumber').addClass('hidden');

        });
    </script>
    <style>
        .hidden {
            display:none;
            margin-left: 27px;
            margin-top: -14px;

        }

        p img {
            margin-top: -59px;
            padding-left: 25px;
        }
        .cash p {
            margin-top: -22px;
            margin-left: 29px;
        }

    </style>
@endpush

