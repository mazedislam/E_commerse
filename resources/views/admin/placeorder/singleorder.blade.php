@extends('layouts/layout')
@section('content')

    <div class="container">
        <!-- MAIN CONTENT -->

                <table class="table">
                    <tr>
                        <td><b>OrderId# {{$singleOrder->order_id
            }} -
                                @if($singleOrder->status==0)
                                    pending
                                @elseif($singleOrder->status==1)
                                    verified
                                @elseif($singleOrder->status==2)
                                    Delivered
                                @endif
                            </b>
                        </td>
                        <td colspan="4"></td>
                    </tr>
                    <tr>
                        <td>
                            <p>About This Order: Order Information</p>
                        </td>
                        <td colspan="4"></td>
                    </tr>
                    <tr>
                        <td>OrderDate {{$singleOrder->created_at}}</td>
                        <td colspan="4"></td>
                    </tr>
                    <tr>
                        <td>
                            <h5 style="color:#ff4500">Shipping Address</h5>
                            <p>{{$singleOrder->name}}</p>
                            <p>{{$singleOrder->address}}</p>
                            <p>{{$singleOrder->email}}</p>
                            <p>{{$singleOrder->phone}}</p>
                        </td>
                        <td colspan="4"></td>
                    </tr>
                    <tr>
                        <td>
                            <h5 style="color:#ff4500">Billing Address</h5>
                            <p>{{$singleOrder->name}}</p>
                            <p>{{$singleOrder->address}}</p>
                            <p>{{$singleOrder->email}}</p>
                            <p>{{$singleOrder->phone}}</p>
                        </td>
                        <td colspan="4"></td>
                    </tr>
                    <tr>
                        <td>
                            <h5 style="color:#ff4500">Paymentmethod</h5>
                            <p>{{$singleOrder->paymentMethod}}</p>
                        </td>
                        <td colspan="4"></td>
                    </tr>
                    <tr>
                        <td>
                            <h5 style="color:#ff4500">ITEMS ORDERED</h5>
                        </td>
                        <td colspan="4"></td>

                    </tr>
                    <tr>
                        <th>Product</th>
                        <th>Sku</th>
                        <th>Qty</th>
                        <th>Price</th>
                        <th>total</th>
                    </tr>
                    <?php $subtotal=0;?>
                    @foreach($orderitems as $orderitem)
                        @foreach($productInfo as $product)
                            @if($orderitem->product_id==$product->id)
                                <?php $total=$orderitem->product_price*$orderitem->product_quantity;
                                $subtotal+=$total;
                                ?>
                                <tr>
                                    <td>{{$product->title}}</td>
                                    <td>{{$product->sku}}</td>
                                    <td>{{$orderitem->product_quantity}}</td>
                                    <td><b>BDT{{ number_format($orderitem->product_price,2,'.',',') }}/-</b></td>
                                    <td><b>BDT{{ number_format($total,2,'.',',') }}/-</b></td>
                                </tr>

                            @endif
                        @endforeach
                    @endforeach
                    <tr>
                        <td colspan="3"></td>
                        <td>Subtotal</td>
                        <td><b>BDT{{ number_format($subtotal,2,'.',',') }}/-</b></td>
                    </tr>

                </table>
                <a href="{{url('myOrder/'.$singleOrder->order_id)}}" style="margin-bottom:15px" class="btn btn-primary"><< Back To My Orders</a>

            </div>
@endsection()



