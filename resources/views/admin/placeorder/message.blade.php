@extends('layouts/layout')
@section('content')

        <h6 class="text-center">YOUR ORDER HAS BEEN RECEIVED</h6>
        <p class="text-center"><b>Thank you for your purchase!</b></p>
        <p class="text-center">your order id # is : @if(session('order_id'))
                        <a  style="color:darkblue;"   href="{{url('placeorder').'/'.session('order_id')}}" >{{session('order_id')}}</a>

        @endif
        </p>

        <p class="text-center">you will receive an order confirmation email with details of your order and a link to track its progress</p>

        <p class="text-center">click <a style="color:darkblue;" href="{{url('pdfInvoice').'/'.session('order_id')}}">here to print</a> a copy order confirmation </p>
    @endsection