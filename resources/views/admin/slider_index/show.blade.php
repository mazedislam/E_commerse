@extends('admin/admin_layout')
@section('page')

    <div class="main">
        <!-- MAIN CONTENT -->
        <div class="main-content">
            <div class="container-fluid">

                <a href="{{ url('/sliders') }}" class="btn btn-primary">List</a>

                <p>Title : {{ $slider->name }}</p>
                <p>Image :
                    <img src="{{ asset('uploads/slider-image/'.$slider->picture) }}">
                </p>

                <p>Title : {{ $slider->price }}</p>
                <p>Title : {{ $slider->link }}</p>
                <p>Title : {{ $slider->htmlblock }}</p>
                <p>Status : {{ $slider->is_active ? 'Yes' : 'No' }}</p>
                <p>Created At : {{ $slider->created_at->diffForHumans()  }}</p>
                <p>Updated At : {{ $slider->updated_at->diffForHumans()  }}</p>

            </div>
        </div>
    </div>

@endsection()

