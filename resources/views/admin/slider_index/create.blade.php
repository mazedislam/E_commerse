@extends('admin/admin_layout')
@section('page')

    <div class="main">
        <!-- MAIN CONTENT -->
        <div class="main-content">
            <div class="container-fluid">

                <div class="col-md-12">
                    <!-- TABLE HOVER -->
                    <div class="panel">
                        <div class="panel-heading">
                            <h3 class="panel-title text-center">Slider Create</h3>
                        </div>
                        <div class="panel-body">

                            <form action="{{ url('/sliders') }}" method="post" enctype="multipart/form-data">
                                @csrf
                                <div class="form-group">
                                    <input type="text" class="form-control" name="name" placeholder="Enter Title" required>
                                </div>
                                <div class="form-group">
                                    <label for="sel1"> data-transition</label>
                                    <select class="form-control" id="sell" name="transition" required>
                                        <option value="data-transition" disabled="">data-transition</option>
                                        <option value="parallaxvertical">parallaxvertical</option>
                                        <option value="fade">fade</option>
                                        <option value="parallaxhorizontal">parallaxhorizontal</option>
                                        <option value="zoomout">zoomout</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <input type="file" class="form-control-file"  name="picture" required>
                                </div>
                                <div class="form-group">
                                    <input type="number" class="form-control"  name="price" placeholder="price" required>
                                </div>
                                <div class="form-group">
                                    <input type="text" class="form-control" name="link" placeholder="link" required>
                                </div>
                                <div class="form-group">
                                    <input type="text" class="form-control" name="htmlblock" placeholder="htmlblock" required>
                                </div>
                                <div class="form-group">
                                    <label>Is Active? </label>
                                    <input type="radio"    name="is_active" value="1" required>Yes
                                    <input type="radio"    name="is_active" value="0" required>No
                                </div>

                                <div class="form-group">
                                    <button type="submit" class="btn btn-success">Add</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>



            </div>
        </div>
    </div>

@endsection()
