@extends('admin/admin_layout')
@section('page')

    <div class="main">
        <!-- MAIN CONTENT -->
        <div class="main-content">
            <div class="container-fluid">

                    <div class="col-md-12">
                        <!-- TABLE HOVER -->
                        <div class="panel">
                            <div class="panel-heading">
                                @if(session('message'))
                                    <h3 class="alert alert-success text-center" id="message">{{ session('message') }}</h3>
                                @endif
                            </div>
                            <div class="panel-body">

                                <table class="table table-hover table-dark">
                                    <thead>
                                    <tr>
                                        <th>name</th>
                                        <th>picture</th>
                                        <th>price</th>
                                        <th>link</th>
                                        <th>htmlblock</th>
                                        <th>is_active</th>
                                        <th colspan="3">Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($sliders as $slider)
                                        <tr>
                                            <td>{{ $slider->name }}</td>
                                            <td><img src="{{ asset('uploads/slider-image/'.$slider->picture) }}" width="200" alt=""></td>
                                            <td>{{ $slider->price }}</td>
                                            <td>{{ $slider->link }}</td>
                                            <td>{{ $slider->htmlblock }}</td>
                                            <td>{{ $slider->is_active ? 'Yes' : 'No' }}</td>
                                            <td><a class='btn btn-outline-primary' href="{{ url('sliders/'.$slider->id) }}">Show</a></td>
                                            <td> <a class="btn btn-outline-success" href="{{ url('sliders/'.$slider->id.'/edit') }}"> Edit </a></td>
                                            <td><form action="{{ url('sliders/'.$slider->id) }}" method="post" style="display: inline">
                                                    @csrf
                                                    {{ method_field('delete') }}
                                                    <button type="submit" class="btn btn-outline-danger" onclick="return confirm('Are You Sure Want To Delete ?')">Delete</button>
                                                </form>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>



            </div>
        </div>
    </div>

@endsection()
@push('script')
    <script>


        jQuery(

            function($) {
                $('#message').fadeOut (550);
                $('#message').fadeIn (550);
                $('#message').fadeOut (550);
                $('#message').fadeIn (550);
                $('#message').fadeOut (550);

            }
        )
    </script>
@endpush

