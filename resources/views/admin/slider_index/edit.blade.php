@extends('admin/admin_layout')
@section('page')

    <div class="main">
        <!-- MAIN CONTENT -->
        <div class="main-content">


            <div class="col-md-12">
                <!-- TABLE HOVER -->
                <div class="panel">
                    <div class="panel-heading">
                        <h3 class="panel-title text-center">Slider Edit</h3>
                    </div>
                    <div class="panel-body">

                        <form action="{{ url('/sliders/'.$edit->id) }}" method="post" enctype="multipart/form-data">
                            @csrf

                            {{method_field('PUT')}}

                            <div class="form-group">
                                <input type="text" class="form-control" name="name" value="{{$edit->name}}" placeholder="Enter Title">
                            </div>
                            <div class="form-group">
                                <img src="{{ asset('uploads/slider-image/'.$edit->picture) }}" width="150">
                                <input type="file" class="form-control-file" name="picture">
                            </div>
                            <div class="form-group">
                                <input type="number" class="form-control"  value="{{$edit->price}}"  name="price" placeholder="price">
                            </div>
                            <div class="form-group">
                                <input type="text" class="form-control" name="link" value="{{$edit->link}}" placeholder="link">
                            </div>
                            <div class="form-group">
                                <input type="text" class="form-control" name="htmlblock" value="{{$edit->htmlblock}}" placeholder="htmlblock">
                            </div>
                            <div class="form-group">
                                <label>Is Active? </label>
                                <input type="radio"  name="is_active" value="1"  @if($edit->is_active==1)  {{'checked'}}@endif >Yes
                                <input type="radio"  name="is_active" value="0"  @if($edit->is_active==0)  {{'checked'}}@endif >No
                            </div>

                            <div class="form-group">
                                <button type="submit" class="btn btn-success">Update</button>
                            </div>
                        </form>

                    </div>
                </div>
            </div>


        </div>
    </div>

@endsection()

