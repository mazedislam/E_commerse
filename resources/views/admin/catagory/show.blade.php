@extends('admin/admin_layout')
@section('page')

    <div class="main">
        <!-- MAIN CONTENT -->
        <div class="main-content">
            <div class="container-fluid">
                <div class="col-md-12">
                    <!-- TABLE HOVER -->
                    <div class="panel">
                        <div class="panel-heading">
                            <h3 class="panel-title"> <a class='btn btn-outline-info' href="{{ url('/catagory/create')}}">Add New</a></h3>
                        </div>
                        <div class="panel-body">

                            <table class="table table-striped">
                                <tr>
                                    <th>title</th>
                                    <th>picture</th>
                                    <th>created</th>
                                    <th>updated</th>
                                </tr>
                            <tr>
                                <td>{{ $catagory->title }}</td>
                                <td> <img src="{{ asset('picture/'.$catagory->picture) }}" width="200" height="100"></td>
                                <td>{{ $catagory->created_at->diffForHumans()  }}</td>
                                <td>{{ $catagory->updated_at->diffForHumans()  }}</td>
                            </tr>
                            </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
@endsection