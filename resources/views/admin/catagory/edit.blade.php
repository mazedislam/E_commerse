@extends('admin/admin_layout')
@section('page')
    <div class="main">
        <!-- MAIN CONTENT -->
        <div class="main-content">
            <div class="container-fluid">
                <div class="col-md-12">
                    <!-- TABLE HOVER -->
                    <div class="panel">
                        <div class="panel-heading">
                            <h3 class="panel-title text-center">Edit</h3>
                        </div>
                        <div class="panel-body">


                <form action="{{ url('/catagory/'.$edit->id) }}" method="post" enctype="multipart/form-data">
                    @csrf
                  {{method_field('PUT')}}


                    <div class="form-group">
                        <input type="text" class="form-control" value="{{$edit->title}}" name="title" placeholder="Enter Title">
                    </div>
                    <div class="form-group">
                        <input type="file"  class="form-control-file"  name="picture"><br>
                        <img src="{{ asset('picture/'.$edit->picture) }}" width="150">
                    </div>

                    <div class="form-group">
                        <button type="submit" class="btn btn-success">Update</button>
                    </div>
                </form>
            </div>
                    </div>
                </div>
        </div>
    </div>
    </div>
@endsection

