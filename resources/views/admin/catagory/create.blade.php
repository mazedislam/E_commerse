@extends('admin/admin_layout')
@section('page')
<div class="main">
        <!-- MAIN CONTENT -->
        <div class="main-content">
            <div class="container-fluid">
                <div class="col-md-12">
                    <!-- TABLE HOVER -->
                    <div class="panel">
                        <div class="panel-heading">
                            <h3 class="panel-title text-center">Category Create</h3>
                        </div>
                        <div class="panel-body">
                            <form action="{{ url('/catagory') }}@yield('edit_id')" method="post" enctype="multipart/form-data">
                                @csrf
                                @yield('method_filed')


                                <div class="form-group">
                                    <input type="text" class="form-control" value="@yield('title')" name="title" placeholder="Enter Title">
                                </div>
                                <div class="form-group">
                                    <input type="file"  class="form-control-file"  name="picture">
                                </div>

                                @yield('picture_show')
                                <div class="form-group">
                                    <button type="submit" class="btn btn-success">Add</button>
                                </div>
                            </form>
                         </div>
                    </div>
                 </div>
            </div>
        </div>
</div>
@endsection
