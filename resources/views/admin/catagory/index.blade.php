@extends('admin/admin_layout')
@section('page')

<div class="main">
    <!-- MAIN CONTENT -->
    <div class="main-content">
        <div class="container-fluid">
        <div class="col-md-12">
            <!-- TABLE HOVER -->
            <div class="panel">
                <div class="panel-heading">
                    @if(session('message'))
                        <h3 class="alert alert-success text-center" id="message">{{ session('message') }}</h3>
                    @endif
                </div>
                <div class="panel-body">

                    <table class="table table-striped">
        <thead>
            <tr>
                <th>Title</th>
                <th>picture</th>
                <th>create</th>
                <th colspan="3">Action</th>
            </tr>
        </thead>
        <tbody>
            @foreach($catagorys as $catagory)
                <tr>
                    <td>{{ $catagory->title }}</td>
                    <td><img src="{{ asset('picture/'.$catagory->picture) }}" width="200" alt=""></td>

                    <td></td>
                    <td><a class='btn btn-outline-primary' href="{{ url('catagory/'.$catagory->id) }}">Show</a></td>
                    <td> <a class="btn btn-outline-success" href="{{ url('catagory/'.$catagory->id.'/edit') }}"> Edit </a></td>
                    <td><form action="{{ url('catagory/'.$catagory->id) }}" method="post" style="display: inline">
                            @csrf
                            {{ method_field('delete') }}
                            <button type="submit" class="btn btn-outline-danger" onclick="return confirm('Are You Sure Want To Delete ?')">Delete</button>
                        </form>
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
                 </div>
            </div>
        </div>
        </div>
    </div>

</div>
@endsection
@push('script')
    <script>


        jQuery(

            function($) {
                $('#message').fadeOut (550);
                $('#message').fadeIn (550);
                $('#message').fadeOut (550);
                $('#message').fadeIn (550);
                $('#message').fadeOut (550);

            }
        )
    </script>
@endpush