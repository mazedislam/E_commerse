@extends("layouts.layout")
@section('title','Products')
@section("content")


    <!-- MAIN -->
    <main>

        <div class="tt-layout tt-sticky-block__parent ">
            <div class="tt-layout__content">
                <div class="container">
                    <div class="tt-listing-page">
                        <div class="tt-page__breadcrumbs">
                            <ul class="tt-breadcrumbs">
                                <li><a href="{{url('home')}}"><i class="icon-home"></i></a></li>
                                <li><a href="{{url('Products')}}">Products</a></li>
                            </ul>
                        </div>
                        <div class="tt-listing-page__view-options tt-vw-opt">
                            <div class="row">
                                <div class="col-xl-3 col-lg-4 col-md-2 col-xs-4">
                                    <div class="tt-vw-opt__grid">
                                        <div class="tt-product-btn-vw" data-control=".tt-product-view">
                                            <label>
                                                <input type="radio" name="product-btn-vw" checked>
                                                <i class="icon-th-large"></i>
                                                <i class="icon-check-empty"></i>
                                            </label>
                                            <label>
                                                <input type="radio" name="product-btn-vw"
                                                       data-view-class="tt-product-view--list">
                                                <i class="icon-th"></i>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tt-listing-page__products tt-layout__mobile-full">

                            <div class="tt-product-view row">
                                @foreach($catagoryproducts as $catagoryproduct)
                                    <div class="col-sm-6 col-xl-4 col-xxl-3">
                                        <div class="tt-product tt-product__view-overlay ttg-text-animation-parent">
                                            <div class="tt-product__image">
                                                <a href="{{url('carts/'.$catagoryproduct->id.'/create')}}">
                                                    <img src="{{asset('product_image/'.$catagoryproduct->picture)}}" data-srcset=""
                                                         data-retina="images/products/product-01.jpg"
                                                         alt="Elegant and fresh. A most attractive mobile power supply.">
                                                </a>
                                                <div class="tt-product__labels">

                                                    <span class="tt-label__sale">Sale</span>
                                                    <span class="tt-label__discount">{{$catagoryproduct->special_price}}</span>

                                                </div>
                                            </div>
                                            <div class="tt-product__hover tt-product__clr-clk-transp">
                                                <div class="tt-product__content">
                                                    <h3>
                                            <span class="ttg-text-animation--emersion">
                                                <a href="listing-with-custom-html-block.html">Headphones</a>
                                            </span>
                                                    </h3>
                                                    <p>
                                            <span class="ttg-text-animation--emersion">
                                                <a href="product-simple-variant-1.html">{{$catagoryproduct->title}}</a>
                                            </span>
                                                    </p>
                                                    <p class="tt-product__description">{{$catagoryproduct->short_description}}</p>
                                                    <div class="ttg-text-animation--emersion">
                                            <span class="tt-product__price">
                                                <span class="tt-price tt-price--sale">
                                                    <span>{{$catagoryproduct->special_price}}</span>
                                                    <span>{{$catagoryproduct->price}}</span>
                                                </span>
                                            </span>
                                                    </div>

                                                    <div class="ttg-text-animation--emersion">
                                                        <div class="tt-product__stars tt-stars">
                                                            <span class="ttg-icon"></span>
                                                            <span class="ttg-icon" style="width:86%;"></span>
                                                        </div>
                                                    </div>

                                                    <div class="ttg-text-animation--emersion">
                                                        <div class="tt-product__buttons">
                                                            <a href="{{url('carts/'.$catagoryproduct->id.'/create')}}" class="tt-btn colorize-btn5  tt-btn__state--active">
                                                                <i class="icon-shop24"></i>

                                                            </a>

                                                            <a href="{{url('products/'.$catagoryproduct->id)}}" class="tt-btn colorize-btn4  tt-btn__state--active">
                                                                <i class="icon-eye"></i>
                                                            </a>
                                                        </div>
                                                    </div>
                                                    <div class="ttg-text-animation--emersion">
                                                        <div class="tt-product__countdown" data-date="{{$catagoryproduct->end_date}}" data-zone="Europe/Madrid"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        </div>

                        <div class=" tt-page__pagination">


                            {{-- <div class="tt-pagination">
 
 
                                 <a href="#" class="btn tt-pagination__prev ttg-hidden">Prev</a>
                                 <div class="tt-pagination__numbs">
 
 
                                 </div>
                                 <a href="http://localhost/E_commerse/public/productlist?page=4" class="btn tt-pagination__next">Next</a>
                             </div>--}}
                        </div>

                    </div>
                </div>
            </div>
        </div>

    </main>



@endsection()

