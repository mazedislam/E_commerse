@extends('layouts/layout')
@section('title','cetagory')
@section('catagory')
    <div class="tt-home__promobox-01">
        <div class="container-fluid ttg-cont-padding--none">
            <div class="row ttg-grid-padding--none">
                <div class="col-sm-12">
                    <div class="row ttg-grid-padding--none">
                        @foreach($catagorys as $catagory)
                            <div class="col-sm-6">
                                <a href="{{url('catagoryproduct/'.$catagory->id)}}" class="tt-promobox
                                                           ttg-text-animation-parent
                                                           ttg-image-translate--right
                                                           ttg-animation-disable--md
                                                           tt-promobox__hover-disable--md">
                                    <div class="tt-promobox__content">
                                        <img src="#" data-srcset="{{url('picture/'.$catagory->picture)}}"
                                             alt="">
                                        <div class="tt-promobox__text"
                                             data-resp-md="md"
                                             data-resp-sm="md"
                                             data-resp-xs="sm">
                                            <div>{{$catagory->title}}</div>
                                        </div>
                                        <div class="tt-promobox__hover tt-promobox__hover--fade">
                                            <div class="tt-promobox__hover-bg"></div>
                                            <div class="tt-promobox__text tt-promobox__point-lg--center">
                                                <div class="ttg-text-animation--emersion">
                                                    <span>{{$catagory->title}}</span>
                                                </div>
                                                <p class="ttg-text-animation--emersion">
                                                    <span><span>{{$catagory->product->count()}}</span> products</span>
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        @endforeach

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

