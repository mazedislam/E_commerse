@extends('layouts/layout')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <img src="{{asset('product_image/'.$product->picture)}}" width="500" height="600"/>
            </div>
            <div class="col-md-6">
                <h5>Quick Overview</h5>
                <ul style="list-style:disc">
                    <li>{{$product->title}}</li>
                    <li>BDT {{$product->special_price}}/-</li>
                    <li>Availability: In Stock</li>
                </ul>

                <form action="{{ url('/carts') }}@yield('edit_id')" method="post" enctype="multipart/form-data">
                    @csrf

                    <div class="form-group">
                        <input  type="hidden" class="form-control" value="{{$id}}" name="sesstion_id" placeholder="">
                    </div>
                    <div class="form-group">
                        <input  type="hidden" class="form-control" value="{{$product->id}}" name="product_id" placeholder="product_id">
                    </div>
                    <label for="quantity">Quantity</label>
                    <div class="tt-counter tt-counter__inner" data-min="1" data-max="10">
                        <input type="text" id="quantity" name="product_quantity" class="form-control" value="1">
                        <div class="tt-counter__control">
                            <span class="icon-up-circle" data-direction="next"></span>
                            <span class="icon-down-circle" data-direction="prev"></span>
                        </div>
                    </div>
                    <div class="form-group">
                        <input  type="hidden" class="form-control" value="{{$product->price}}" name="product_price" placeholder="product_price">
                    </div>

                    <div class="form-group">
                        <button type="submit" class="btn btn-outline-danger"><i class="fa fa-shopping-bag"></i> BUY NOW</button>
                    </div>

                </form>
            </div>
        </div>
    </div>
@endsection