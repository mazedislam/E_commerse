@extends('layouts/layout')
@section('content')
<div class="container">

        @if(session()->has('message'))
            <h5 class="text-center alert-warning fade-out">
            {{session()->get('message')}}
            </h5>
        @endif
    <h1>Cart List</h1>
    <table class="table">
        <thead>
            <tr>
                <th>&nbsp;</th>
                <th>Productn Name</th>
                <th>Qty</th>
                <th>Unit Price</th>
                <th>Subtotal</th>
                <th>&nbsp;</th>
            </tr>
        </thead>
        <tbody>
        <?php
        $sum=0;?>
            @foreach($carts as $cart)
                @foreach($products as $product)
                    @if($cart->product_id==$product->id)

                <tr>
                    <?php
                    $total=$cart->product_quantity*$product->price;
                    $sum =$total+$sum;
                    ?>

                    <td><img src="product_image/{{$product->picture}}" style="width:30%"/></td>
                    <td><a href="{{ url('products/'.$cart->product_id) }}">{{ $product->title }}</a></td>
                    <td>

                        <form action="{{ url('/carts/'.$cart->id) }}" method="post" enctype="multipart/form-data">
                            @csrf
                            {{method_field('PUT')}}

                            <input  type="number" class="form-control" value="{{ $cart->product_quantity }}" name="product_quantity" placeholder="product_quantity">
                            <button  type="submit" class="btn btn-success">Update</button>
                        </form>
                    </td>
                    <td>BDT {{ number_format($product->price ,2,'.',',').'/-'}}</td>
                    <td>BDT {{  number_format($total,2,'.',',').'/-' }}


                    </td>
                    <td><form action="{{ url('carts/'.$cart->id) }}" method="post" style="display: inline">
                            @csrf
                            {{ method_field('delete') }}
                            <button type="submit" class="btn btn-outline-danger" onclick="return confirm('Are You Sure you Want To Remove product ?')">Remove from cart</button>
                        </form>
                    </td>
                </tr>

                    @endif
                @endforeach
            @endforeach
        <tr>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td colspan="2">
                SubTotal   <b>BDT{{ number_format($sum,2,'.',',') }}</b>
                GrandTotal <b>BDT{{ number_format($sum,2,'.',',') }}</b>
            </td>
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td><a class='btn btn-outline-info' href="{{ url('/Products')}}">Add More Products</a></td>
            <td><a class='btn btn-outline-info' href="{{ url('/placeorder/create')}}">CheckOut</a></td>
        </tr>
        </tbody>
    </table>
            <script>
                $('.fade-out').delay(300).slideUp(400);
            </script>
</div>
    @endsection()