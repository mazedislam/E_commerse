@extends('layouts/layout')
@section('content')
    <div class="container">
        <a href="{{ url('/carts') }}" class="btn btn-primary">List</a>
        
        <p>ProductId : {{ $cartview->product_id }}</p>
        <p>Product Quantity : {{ $cartview->product_quantity }}</p>
        <p>product price : {{ $cartview->product_price }}</p>
        <p>Created At : {{ $cartview->created_at->diffForHumans()  }}</p>
        <p>Updated At : {{ $cartview->updated_at->diffForHumans()  }}</p>
    </div>
@endsection


