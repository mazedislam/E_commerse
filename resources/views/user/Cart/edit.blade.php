@extends('layouts.layout')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <img src="{{asset('product_image/'.$product_id->picture)}}" width="500" height="600"/>
            </div>
            <div class="col-md-6">
                <p>Availability: In Stock</p>

                <form action="{{ url('/carts/'.$cartedit->id) }}" method="post" enctype="multipart/form-data">
                    @csrf
                    {{method_field('PUT')}}

                    <div class="form-group">
                        <input  type="hidden" class="form-control" value="{{$id}}" name="sesstion_id" placeholder="">
                    </div>
                    <div class="form-group">
                        <input  type="hidden" class="form-control" value="{{$cartedit->product_id}}" name="product_id" placeholder="product_id">
                    </div>
                    <label for="quantity">Quantity</label>
                    <div class="tt-counter tt-counter__inner" data-min="1" data-max="10">
                        <input type="text" id="quantity" name="product_quantity" class="form-control" value="{{$cartedit->product_quantity}}">
                        <div class="tt-counter__control">
                            <span class="icon-up-circle" data-direction="next"></span>
                            <span class="icon-down-circle" data-direction="prev"></span>
                        </div>
                    </div>
                    <div class="form-group">
                        <input  type="hidden" class="form-control" value="{{$cartedit->product_price}}" name="product_price" placeholder="product_price">
                    </div>

                    <div class="form-group">
                        <button type="submit" class="btn btn-outline-danger"><i class="fa fa-shopping-bag"></i> BUY NOW</button>
                    </div>

                </form>
            </div>
        </div>
    </div>
@endsection