@extends('layouts/layout')
@section('content')
    <main>

        <div class="tt-layout tt-sticky-block__parent ">
            <div class="tt-layout__content">
                <div class="container">
                    <div class="tt-page__breadcrumbs">
                        <ul class="tt-breadcrumbs">
                            <li><a href="../main_index.blade.php"><i class="icon-home"></i></a></li>
                            <li><a href="../main_index.blade.php?page=listing-collections.html">Collections</a></li>
                            <li><span>Audio</span></li>
                        </ul>
                    </div>
                    <div class="tt-empty">
                        <i class="tt-empty__icon"><img src="images/empty/empty-shopping-cart.svg" alt="Image name"></i>
                        <div class="tt-page__name text-center">
                            <h1>Shopping Cart is Empty</h1>
                            <p>You have no items in your shopping cart.</p>
                        </div>
                        <div class="tt-empty__btn">
                            <a href="{{url('/')}}" class="btn">Continue Shopping</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </main>


@endsection