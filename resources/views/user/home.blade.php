@extends("layouts.layout")

@section('title','ShopHub')

@section("content")
    <main>
        <div class="tt-layout tt-sticky-block__parent tt-layout__fullwidth">
            <div class="tt-layout__content">
                <div class="container">
                    <div class="tt-sr" data-layout="fullscreen">
                        <div class="tt-sr__content" data-version="5.3.0.2">
                            <ul>
                                @foreach($sliders as $slider)

                                <li data-index="rs-304{{$slider->id}}"
                                    data-transition="{{$slider->transition}}"
                                    data-fstransition="fade" data-fsmasterspeed="1500" data-fsslotamount="7"
                                    data-masterspeed="1500">
                                    <img src="{{ asset('uploads/slider-image/'.$slider->picture) }}"
                                         alt="Image name"
                                         class="rev-slidebg"
                                         data-bgposition="center center"
                                         data-bgfit="cover"
                                         data-bgrepeat="no-repeat"
                                         data-bgparallax="8">
                                    <div class="tp-caption
                                rs-parallaxlevel-3
                                tt-sr__text"
                                         data-frames='[{"delay":0,"speed":2000,"frame":"0","from":"y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;","to":"o:1;","ease":"Power4.easeInOut"},{"delay":"wait","speed":1000,"frame":"999","to":"y:[-100%];opacity:0;","ease":"Power3.easeInOut"}]'
                                         data-x="left"
                                         data-y="center"
                                         data-whitespace="nowrap"
                                         data-width="['auto']"
                                         data-height="['auto']"
                                         data-hoffset="76">
                                        <div>{{$slider->name}}</div>
                                        <span>${{$slider->price}}</span>
                                        <p></p>
                                        <a href="#" class="tt-btn tt-btn--big tt-btn--hover-effect colorize-btn7">
                                            <i class="icon-shop24"></i>
                                        </a>
                                    </div>
                                </li>

                                @endforeach

                            </ul>
                        </div>
                    </div>


                </div>
            </div>
        </div>

    </main>
@endsection
@section('catagory')
    <div class="tt-home__promobox-01">
        <div class="container-fluid ttg-cont-padding--none">
            <div class="row ttg-grid-padding--none">
                <div class="col-sm-12">
                    <div class="row ttg-grid-padding--none">
                        @foreach($catagorys as $catagory)
                        <div class="col-sm-6">
                            <a href="{{url('catagoryproduct/'.$catagory->id)}}" class="tt-promobox
                                                           ttg-text-animation-parent
                                                           ttg-image-translate--right
                                                           ttg-animation-disable--md
                                                           tt-promobox__hover-disable--md">
                                <div class="tt-promobox__content">
                                    <img src="#" data-srcset="{{url('picture/'.$catagory->picture)}}"
                                         alt="">
                                    <div class="tt-promobox__text"
                                         data-resp-md="md"
                                         data-resp-sm="md"
                                         data-resp-xs="sm">
                                        <div>{{$catagory->title}}</div>
                                    </div>
                                    <div class="tt-promobox__hover tt-promobox__hover--fade">
                                        <div class="tt-promobox__hover-bg"></div>
                                        <div class="tt-promobox__text tt-promobox__point-lg--center">
                                            <div class="ttg-text-animation--emersion">
                                                <span>{{$catagory->title}}</span>
                                            </div>
                                            <p class="ttg-text-animation--emersion">
                                                <span><span>{{$catagory->product->count()}}</span> products</span>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>
                            @endforeach

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('shipping_info')
    <div class="tt-home__shipping-info-01">
        <div class="tt-shp-info tt-shp-info__design-01">
            <div class="row ttg-grid-padding--none ttg-grid-border">
                <div class="col-lg-4">
                    <a href="#" class="tt-shp-info__section ">
                        <i class="icon-phone"></i>
                        <div class="tt-shp-info__strong">+880 183 461 3761 </div>
                        <p>Toll-free hotline. 7 days a week from <strong><em>10.00 a.m. to 6.00
                                    p.m.</em></strong></p>
                    </a>
                </div>
                <div class="col-lg-4">
                    <a href="#" class="tt-shp-info__section ">
                        <i class="icon-box"></i>
                        <div class="tt-shp-info__strong">Free Shipping</div>
                        <p>Shipping prices for any form of delivery and order’s cost is constant - $49. A
                            free shipping is available for orders <span>more than $99.</span></p>
                    </a>
                </div>
                <div class="col-lg-4">
                    <a href="#" class="tt-shp-info__section ">
                        <i class="icon-left"></i>
                        <div class="tt-shp-info__strong">Returns and Exchanges</div>
                        <p>Any goods, that was bought in our online store, can be returned during <span>30 days</span>
                            since purchase date.</p>
                    </a>
                </div>
            </div>
        </div>
    </div>
@endsection

