@extends("layouts/layout")
@section('title',$product->title)
@section("content")
    <main>
<div class="tt-layout tt-sticky-block__parent ">
    <div class="tt-layout__content">
        <div class="container">
            <div class="tt-product-page">
                <div class="tt-product-page__breadcrumbs">
                    <ul class="tt-breadcrumbs">
                        <li><a href="../../layout/main_index.blade.php"><i class="icon-home"></i></a></li>
                        <li><a href="../../layout/main_index.blade.php?page=listing-collections.html">Collections</a></li>
                        <li><span>{{$product->title}}</span></li>
                    </ul>
                </div>


                <div class="tt-product-head tt-sticky-block__parent">
                    <div class="tt-product-head__sticky tt-sticky-block tt-layout__mobile-full">
                        <div class="tt-product-head__images tt-sticky-block__inner  ">
                            <div class="tt-product-head__image-main">
                                <img src="{{asset('product_image/'.$product->picture)}}"
                                     data-zoom-image="{{asset('product_image/'.$product->picture)}}" data-full="#"
                                     alt="Image name">
                            </div>

                        </div>
                    </div>
                    <div class="tt-product-head__sticky tt-sticky-block">
                        <div class="tt-product-head__info tt-sticky-block__inner">
                         {{--   <form action="#">--}}
                                <div class="tt-product-head__info-head">
                                    <div class="tt-product-head__index">SKU: <span>{{$product->sku}}</span></div>
                                </div>
                                <div class="tt-product-head__name">{{$product->title}}</div>
                                <div class="tt-product-head__review">
                                    <div class="tt-product-head__stars
                            tt-stars">
                                        <span class="ttg-icon"></span>
                                        <span class="ttg-icon" style="width: 70%;"></span>
                                    </div>
                                    <div class="tt-product-head__review-count"><a href="#">{{count($reviewProducts)}}
                                            @if(count($reviewProducts)==0 || count($reviewProducts)==1)
                                                Review
                                            @else
                                                Reviews
                                            @endif
                                        </a></div>
                                    <div class="tt-product-head__review-add"><a href="#">Add Your Review</a></div>
                                </div>
                                <div class="tt-product-head__price">
                                   <span style="font-size:30px"> BDT  {{number_format($product->price,2,'.',',').'/-'}}</span>
                                </div>
                                <div class="tt-product-head__options">
                                    <div class="prdbut__options prdbut__options--page">
                                    </div>
                                </div>
                                <div class="tt-product-head__control">
                                    <form action="{{url('/carts')}}" method="post" enctype="multipart/form-data">
                                        @csrf

                                        <div class="form-group">
                                            <input  type="hidden" class="form-control" value="{{$id}}" name="sesstion_id" placeholder="">
                                        </div>
                                        <div class="form-group">
                                            <input  type="hidden" class="form-control" value="{{$product->id}}" name="product_id" placeholder="product_id">
                                        </div>
                                    <div class="tt-product-head__counter tt-counter tt-counter__inner" data-min="1"
                                         data-max="10">
                                        <input type="text" name="product_quantity" class="form-control" value="1">
                                        <div class="tt-counter__control">
                                            <span class="icon-up-circle" data-direction="next"></span>
                                            <span class="icon-down-circle" data-direction="prev"></span>
                                        </div>
                                    </div>
                                        <div class="form-group">
                                            <input  type="hidden" class="form-control" value="{{$product->price}}" name="product_price" placeholder="product_price">
                                        </div>


                                        <button class=" tt-btn tt-btn--cart colorize-btn6" type="submit">
                                            <i class="icon-shop24"></i>
                                            <span> Add to Cart</span>
                                        </button>

                                    </form>

                                </div>
                        </div>
                    </div>
                </div>
                <div class="tt-product-page__tabs tt-tabs tt-layout__mobile-full" data-tt-type="horizontal">
                    <div class="tt-tabs__head">
                        <div class="tt-tabs__slider">
                            <div class="tt-tabs__btn" data-active="true"><span>Description</span></div>
                            <div class="tt-tabs__btn"><span>Additional</span></div>
                            <div class="tt-tabs__btn"><span>Shipping Information</span></div>
                            <div class="tt-tabs__btn" data-tab="review"><span>Reviews</span></div>
                        </div>
                        <div class="tt-tabs__btn-prev"></div>
                        <div class="tt-tabs__btn-next"></div>
                        <div class="tt-tabs__border"></div>
                    </div>
                    <div class="tt-tabs__body tt-tabs-product">
                        <div>
                            <span>Description <i class="icon-down-open"></i></span>
                            <div class="tt-tabs__content">
                                <div class="tt-tabs__content-head">Description</div>
                                <p><?php echo $product->description ?></p>

                            </div>
                        </div>
                        <div>
                            <span>Additional <i class="icon-down-open"></i></span>
                            <div class="tt-tabs__content">
                                <div class="tt-tabs__content-head">Additional</div>
                                 {!! $product->additional_information !!}
                            </div>
                        </div>
                        <div>
                            <span>Shipping Information <i class="icon-down-open"></i></span>
                            <div class="tt-tabs__content">
                                <p style="color:black;font-weight: bold">Chittagong City: 40 TK<br>
                                Outside Chittagong City: 60 TK</p>
                                <div class="panel-body">
                                    <p style="color:red;font-weight: bold">200 tk or 10% (which ever is higher) advance payment is required for outside Dhaka delivery.
                                        The maximum order value for COD is BDT 10,000. If its above BDT 10,000, required 10% advance payment</p>
                                </div>
                            </div>
                        </div>
                        <div>
                            <span>Reviews <i class="icon-down-open"></i></span>
                            <div class="tt-tabs__content">
                                <div class="tt-tabs__content-head">Customer Reviews</div>
                                <div class="tt-tabs-product__review tt-review">
                                    <div class="tt-review__head">
                                        <div class="tt-review__head-stars tt-stars">
                                            <span class="ttg-icon"></span>
                                            <span class="ttg-icon" style="width: 70%;"></span>
                                        </div>
                                        <span>Based on 2 review</span>
                                        <a href="#">Write a review</a>
                                    </div>
                                    <div class="tt-review__form">
                                        <span>Write a review</span>



                                        <form action="{{ url('/reviews') }}" method="post">
                                            @csrf
                                            <div class="row">
                                                <input type="hidden" class="form-control" value="{{$product->id}}" name="product_id" placeholder="product_id">
                                                <div class="col-sm-4"><label for="reviewName">Name:</label></div>
                                                <div class="col-sm-8">
                                                    <input type="text" id="reviewName" name="name" class="form-control"
                                                           placeholder="Enter your name">
                                                </div>
                                            </div>
                                            <div class="row ttg-mt--20">
                                                <div class="col-sm-4"><label for="reviewEmail">E-mail:</label></div>
                                                <div class="col-sm-8">
                                                    <input type="text" id="reviewEmail" name="Email" class="form-control"
                                                           placeholder="John.smith@example.com">
                                                </div>
                                            </div>
                                         {{--   <input type="hidden" value="{{date('Y/m/d')}}" name="start_date">--}}
                                            <div class="row ttg-mt--20">
                                                <div class="col-sm-4"><label>E-Rating:</label></div>
                                                <div class="col-sm-8">
                                                    <div class="tt-review__form-stars tt-stars tt-stars__input">
                                                        <span class="ttg-icon"></span>
                                                        <span class="tt-stars__set ttg-icon"
                                                              style="width:70%;"></span>
                                                        <input type="hidden" id="reviewStars" name="rating">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row ttg-mt--20">
                                                <div class="col-sm-4"><label for="reviewTitle">Review Title:</label>
                                                </div>
                                                <div class="col-sm-8">
                                                    <input type="text" id="reviewTitle" name="review_title" class="form-control"
                                                           placeholder="Give your review a title">
                                                </div>
                                            </div>
                                            <div class="row ttg-mt--20">
                                                <div class="col-sm-4"><label for="reviewBody">Body of Review:</label></div>
                                                <div class="col-sm-8">
                                                    <textarea id="reviewBody" name="body_review" class="form-control">Wtite your comments here</textarea>
                                                </div>
                                            </div>
                                            <div class="row ttg-mt--20">
                                                <div class="col-sm-8 offset-sm-4">
                                                    <button type="submit" class="btn">Submit Review</button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                    <div class="tt-review__comments">
                                        @if(count($reviewProducts)>0)
                                        @foreach($reviewProducts as $reviewProduct)
                                        <div>
                                            <div class="tt-stars">
                                                <span class="ttg-icon"></span>
                                                <span class="ttg-icon" style="width:{{$reviewProduct->rating}}%;"></span>
                                            </div>
                                            <div class="tt-review__comments-title">
                                                {{$reviewProduct->review_title}}
                                            </div>
                                            <span>{{$reviewProduct->name}}<span> </span>{{$reviewProduct->created_at}}</span>
                                            <p>{{$reviewProduct->body_review}}</p>
                                        </div>
                                        @endforeach
                                        @endif
                                    </div>

                                </div>
                            </div>
                        </div>
                        <div>

                        </div>
                    </div>
                </div>

            </div>

            <script type="application/ld+json">
{
  "@context": "http://schema.org",
  "@type": "Product",
  "aggregateRating": {
    "@type": "AggregateRating",
    "ratingValue": "3.5",
    "reviewCount": "11"
  },
  "description": "0.7 cubic feet countertop microwave. Has six preset cooking categories and convenience features like Add-A-Minute and Child Lock.",
  "name": "Kenmore White 17\" Microwave",
  "image": "kenmore-microwave-17in.jpg",
  "offers": {
    "@type": "Offer",
    "availability": "http://schema.org/InStock",
    "price": "55.00",
    "priceCurrency": "USD"
  },
  "review": [
    {
      "@type": "Review",
      "author": "Ellie",
      "datePublished": "2011-04-01",
      "description": "The lamp burned out and now I have to replace it.",
      "name": "Not a happy camper",
      "reviewRating": {
        "@type": "Rating",
        "bestRating": "5",
        "ratingValue": "1",
        "worstRating": "1"
      }
    },
    {
      "@type": "Review",
      "author": "Lucas",
      "datePublished": "2011-03-25",
      "description": "Great microwave for the price. It is small and fits in my apartment.",
      "name": "Value purchase",
      "reviewRating": {
        "@type": "Rating",
        "bestRating": "5",
        "ratingValue": "4",
        "worstRating": "1"
      }
    }
  ]
}

                </script>
        </div>
    </div>
</div>


</main>


@endsection()
