@extends("layouts/layout")
@section('title','offerzone')
@section("content")
    <div class="container-fluid ttg-cont-padding--none">
        <div class="row tt-product-view ttg-grid-padding--none">
            @foreach($offers as $offer)
                <div class="col-sm-6 col-xl-3">
                    <div class="tt-product tt-product__view-overlay ttg-text-animation-parent">
                        <div class="tt-product__image">
                            <a href="product-simple-variant-1.html">
                                <img src="{{asset('product_image/'.$offer->picture)}}" data-srcset="{{asset('product_image/'.$offer->picture)}}"
                                     data-retina="{{asset('product_image/'.$offer->picture)}}"
                                     alt="Elegant and fresh. A most attractive mobile power supply.">
                            </a>
                            <div class="tt-product__labels">
                                <span class="tt-label__discount">{{$offer->special_price}}</span>
                            </div>
                        </div>
                        <div class="tt-product__hover tt-product__clr-clk-transp">
                            <div class="tt-product__content">
                                <h3>
                                            <span class="ttg-text-animation--emersion">
                                                <a href="listing-with-custom-html-block.html">{{$offer->title}}</a>
                                            </span>
                                </h3>
                                <p>

                                </p>
                                <p class="tt-product__description">Sed ut perspiciatis unde omnis iste natus
                                    error sit voluptatem accusantium doloremque laudantium, totam rem aperiam,
                                    eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae
                                    dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit
                                    aspernatur aut odit aut fugit, sed quia consequuntur magni dolores.</p>
                                <div class="ttg-text-animation--emersion">
                                            <span class="tt-product__price">
                                                <span class="tt-price tt-price--sale">
                                                    <span>{{$offer->special_price}}</span>
                                                    <span>{{$offer->price}}</span>
                                                </span>
                                            </span>
                                </div>

                                <div class="ttg-text-animation--emersion">
                                    <div class="tt-product__stars tt-stars">
                                        <span class="ttg-icon"></span>
                                        <span class="ttg-icon" style="width:86%;"></span>
                                    </div>
                                </div>
                               {{-- <div class="tt-product__option">
                                    <div class="prdbut__options prdbut__options--list">
                                        <div class="ttg-text-animation--emersion">
                                            <div class="prdbut__option prdbut__option--color prdbut__option--design-color">
                                                        <span class="prdbut__val prdbut__val--white active default">
                                                            <span>White</span>
                                                        </span>
                                                <span class="prdbut__val prdbut__val--black">
                                                            <span>Black</span>
                                                        </span>
                                                <span class="prdbut__val prdbut__val--red">
                                                            <span>Red</span>
                                                        </span>
                                            </div>
                                        </div>
                                        <div class="ttg-text-animation--emersion">
                                            <div class="prdbut__option prdbut__option--size prdbut__option--design-bg">
                                                        <span class="prdbut__val prdbut__val--m active default">
                                                            <span>M</span>
                                                        </span>
                                                <span class="prdbut__val prdbut__val--l">
                                                            <span>L</span>
                                                        </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>--}}
                                <div class="ttg-text-animation--emersion">
                                    <div class="tt-product__buttons">
                                        <a href="{{url('carts/'.$offer->id.'/create')}}" class="tt-btn colorize-btn5  tt-btn__state--active">
                                            <i class="icon-shop24"></i>
                                        </a>
                                        <a href="{{url('products/'.$offer->id)}}" class="tt-btn colorize-btn4  tt-btn__state--active">
                                            <i class="icon-eye"></i>
                                        </a>
                                    </div>
                                </div>
                                <div class="ttg-text-animation--emersion">
                                    <div class="tt-product__countdown" data-date="{{$offer->end_date}}" data-zone="Europe/Madrid"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </div>

@endsection
