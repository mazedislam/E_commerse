<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>

<table border="1px" class="table table-condensed" style="box-shadow: 1px 10px 10px black;">
    <tr>
        <th>Product Name</th>
        <th>Qty</th>
        <th>price</th>
        <th>sku</th>
        <th>meta keyword</th>
        <th>total</th>
    </tr>
    <?php $subtotal=0;?>
    @foreach ($orderItems as $orderItem)
        @foreach ($productItems as $productItem)
            <tr>
                @if($orderItem->product_id == $productItem->id)
                    <?php
                    $total=$orderItem->product_price*$orderItem->product_quantity;
                        $subtotal+=$total;
                    ?>
                    <td>{{$productItem->title}}</td>
                    <td>{{$orderItem->product_quantity}}</td>
                    <td>{{number_format($orderItem->product_price,2,'.',',').'/-'}}</td>
                    <td>{{$productItem->sku}}</td>
                    <td>{{$productItem->meta_keyword}}</td>
                    <td>{{number_format($total ,2,'.',',').'/-'}}</td>
                @endif
            </tr>
        @endforeach
    @endforeach
    <tr>
        <td colspan="4"></td>
        <td>SubTotal</td>
        <td>{{number_format($subtotal,2,'.',',').'/-'}}</td>
    </tr>
</table>

</body>
</html>