<?php


/*Route::get('/home', function () {
    return view('admin/index');
});*/

Route::get('/', 'PublicController@index');
Route::get('/Home','PublicController@index');

Route::get('/mogo', function () {
    return view('user/products/mogo');
});

Route::get('/admin', function () {
    return view('admin/index');
});

/*Main_Index_File_Routing*/


Route::resource('/sliders', 'Slidercontroller');
Route::get('/slider_list', 'Slidercontroller@slider_list');

Route::resource('/staticblock', 'Staticcontroller');
Route::resource('/reviews', 'Reviewcontroller');
Route::resource('/catagory', 'Catagorycontroller');
Route::get('/catagoryproduct/{id}', 'Catagorycontroller@catagoryProduct');
Route::get('/Catagory', 'Catagorycontroller@catagoryList');
Route::resource('/products', 'Productcontroller');
Route::get('/Products', 'Productcontroller@productList');

Route::get('/Offerzone', 'Productcontroller@offerZone');
Route::resource('/products', 'Productcontroller');
Route::resource('/pages', 'PageController');
Route::resource('/subscribe', 'SubscribeController');
Route::resource('/navigation', 'NavigationController');
Route::resource('/carts', 'CartController');
Route::get('/carts/{id}/create', 'CartController@createCart');
Route::resource('/placeorder', 'PlaceorderController');
Route::get('/message', 'PlaceorderController@message');
Route::get('/myOrder/{order_id}', 'PlaceorderController@myOrder');
Route::resource('/orders', 'OrderController');
Route::get('/pdfInvoice/{orderId}', 'OrderController@pdfInvoice');

Route::group(['middleware'=>'auth'], function () {

});





Auth::routes();

Route::get('/admin', 'HomeController@index')->name('home');
